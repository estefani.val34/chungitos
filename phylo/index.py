'''
    Voy a intentar hacer un script en el que tengo un fichero con varias cadenas de ADN
    y me crea el arbol filogenético
'''

import sys
import re
from io import StringIO
from Bio import Phylo
from Bio.Phylo.TreeConstruction import DistanceCalculator
from Bio import AlignIO
from Bio.Phylo.TreeConstruction import DistanceTreeConstructor
from Bio import SeqIO
import networkx, pylab

FILEFASTA = sys.argv[1]
FILEPHY = "media/contents.phy"

def from_multifasta_to_phylip(file):
    """
        This functiion reads all sequences from a multifasta an creates a file with format phy;
        this format is necessary to be read by function AlignIO and make alignment.
    """
    secuences = list(SeqIO.parse(file, "fasta"))
    quant = len(secuences)
    lon = len(secuences[1])

    for sec in secuences:
        if len(sec) < lon:
            lon = len(sec)

    output = StringIO()
    dif = 0
    output.write(" " + str(quant) + " " + str(lon) + "\n")
    for sec in secuences:
        match = re.search("([0-9]+$)", sec.id)
        name = match.group(1)
        if len(name) < 10 and name != "":
            dif_sp = 10 - len(name)
            for c in range(dif_sp):
                name = name + " "
        elif name is "":
            if len(str(dif)) is 1:
                name = "XXXXXXXXX" + str(dif)
            elif len(str(dif)) is 2:
                name = "XXXXXXXX" + str(dif)
            elif len(str(dif)) is 3:
                name = "XXXXXXX" + str(dif)
            dif = dif + 1
        output.write(name)
        output.write("\t")
        output.write(str(sec.seq)[1:lon] + "\n")
    contents = output.getvalue()
    output.close()
    with open(FILEPHY, "w+") as file_contents:
        file_contents.write(contents)


def make_tree(file):
    '''
        Take the multifasta information from param "file", changes the format using
        fromMultifastaToPhylp and writes it in another type of file(phylip).
        Ant then, this function takes this file and constructs
        the philogenyc tree with another functions from Biopython.
    '''
    from_multifasta_to_phylip(file)
    aln = AlignIO.read(FILEPHY, 'phylip')
    calculator = DistanceCalculator('identity')
    distance_matrix = calculator.get_distance(aln)
    constructor = DistanceTreeConstructor(calculator, 'nj')
    tree = constructor.build_tree(aln)
    tree = constructor.upgma(distance_matrix)
    #Phylo.draw(tree)
    #Phylo.draw_ascii(tree)
    #with open('media/asciout.txt', 'w+') as fh:
    #    fh.write(Phylo.draw_ascii(tree))
    #print(tree)
    print(type(str(Phylo.draw_ascii(tree))))
    with open("media/asciout.txt", "w+") as fh:
        fh.write(str(Phylo.draw_ascii(tree)))


def handle_uploaded_file(f):
    with open('media/'+str(f), 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

#make_tree(FILEFASTA)
