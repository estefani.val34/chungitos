"""
Functions used in the function search_term_ncbi of views.py
Currently we support the following 3 function:

1. **Search_something** - Manage the Search Term section of the menu
(jump to section in [[models.py]])
2. **Random_string** - Generate random names unique
3. **Search_something_structure** - Manage the Search Term section of
the menu when the user chooses the NCBI Structure database.

References:

NCBI : https://www.ncbi.nlm.nih.gov/

Entrez : https://www.ncbi.nlm.nih.gov/Class/MLACourse/Original8Hour/Entrez/

"""

import random
import os
import os.path
import string
from django.core.files.storage import FileSystemStorage
from Bio import Entrez
# defined in [[models.py]]
from .models import Url


# === Search_something ===
def search_something(term_search, max_search, user, user_name, database):
    """
    Manage the Search Term section of the menu.
    With Entrez you search for the term entered in the NCBI.
    The file is downloaded and saved in our database.

    Call functions: random_string ()

    Arguments:

      term_search {str} -- term entered in the input Term

      max_search {int} -- number of downloads

      user {str} -- user_id

      user_name {str} -- the name of the user making the request

      database {str} -- in which NCBI database do you want to search

    Returns:

      [] -- save file

    """

    os.makedirs('media/'+user, exist_ok=True)
    urls = []

    Entrez.email = "estefani.val34@gmail.com"

    with Entrez.esearch(db=database,
                        term=term_search,
                        idtype="acc",
                        retmax=max_search) as response:
        res = Entrez.read(response)

    if res['Count'] != '0':

        ide = ",".join(res["IdList"])
        with Entrez.epost(database, id=ide) as response:
            epost = Entrez.read(response)

        query_key = epost['QueryKey']

        web_env = epost['WebEnv']

        acc_arr = res['IdList']
        acc_arr2 = acc_arr.copy()

        with Entrez.efetch(db=database,
                           retmode="text",
                           rettype="gb",
                           retstart=0,
                           retmax=1,
                           webenv=web_env,
                           query_key=query_key,
                           idtype="acc") as response:
            acc_arr[0] = response.read()

        lenght = len(acc_arr)
        for i in range(1, lenght, 1):
            with Entrez.efetch(db=database,
                               retmode="text",
                               rettype="gb",
                               retstart=i,
                               retmax=i,
                               webenv=web_env,
                               query_key=query_key,
                               idtype="acc") as response:
                acc_arr[i] = response.read()

        lenght2 = len(acc_arr2)
        filesy = FileSystemStorage()
        file_suffix = random_string(15)
        for i in range(lenght2):
            with open(os.path.join('media/'+user, acc_arr2[i]+file_suffix+'.gb'), 'w') as gb_file:
                path = os.path.join(
                    'media/'+user, acc_arr2[i]+file_suffix+'.gb')
                gb_file.write(acc_arr[i])
                url = filesy.url(gb_file.name)
                Url.objects.create(
                    title=acc_arr2[i], user=user_name, gb=path)
                urls.append(url)
    else:
        urls.append("Not found results.")
    return urls


# === Random_string ===
def random_string(string_lenght=8):
    """
    Generate random names unique

    Keyword Arguments:

      string_lenght {int} -- name (default: {8})

    Returns:

      [str] -- random name
    """

    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(string_lenght))


# === Search_something_structure ===
def search_something_structure(term_search, max_search, user, database):
    """
    Manage the Search Term section of the menu when the user chooses the NCBI Structure database.
    With Entrez you search for the term entered in the NCBI.
    It shows the links of the proteins in 3D.

    Arguments:

      term_search {str} -- term entered in the input Term

      max_search {int} -- number of downloads

      user {str} -- user_id

      database {str} -- Structure

    Returns:

        [] -- links
    """

    os.makedirs('media/'+user, exist_ok=True)
    urls = []

    Entrez.email = "estefani.val34@gmail.com"

    with Entrez.esearch(db=database,
                        term=term_search,
                        idtype="acc",
                        retmax=max_search) as response:
        res = Entrez.read(response)

    if res['Count'] != '0':
        urls = res["IdList"]
    else:
        urls.append("Not found results.")
    return urls
