"""
Admin Site
Contains 3 classes:

1. **ProteinAdmin** - show model Protein
2. **PBDAdmin** - Show model PBD
3. **UrlAdmin** - Show model Url

References:

Creating an admin user : https://docs.djangoproject.com/en/3.0/intro/tutorial02/

"""

from django.contrib import admin

# defined in [[models.py]]
from .models import Protein, PBD, Url


# === ProteinAdmin ===
class ProteinAdmin(admin.ModelAdmin):
    """
    Show Protein table in Admin.
    """

    list_display = ('entry', 'entry_name', 'organism')
    list_display_links = ('entry', 'entry_name')
    list_filter = ('entry', 'organism')

admin.site.register(Protein, ProteinAdmin)


# === PBDAdmin ===
class PBDAdmin(admin.ModelAdmin):
    """
    Show PBD table in Admin.
    """

    list_display = ('id_pbd', 'entry')
    list_display_links = ('id_pbd', 'entry')
    list_filter = ('id_pbd', 'entry')

admin.site.register(PBD, PBDAdmin)


# === UrlAdmin ===
class UrlAdmin(admin.ModelAdmin):
    """
    Show Url table in Admin.
    """

    list_display = ('title', 'types', 'user', 'gb')
    list_display_links = ('title', 'types', 'gb')
    list_filter = ('title', 'types', 'user')

admin.site.register(Url, UrlAdmin)
