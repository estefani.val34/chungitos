"""
Models to create the tables in the database.
Contains 3 models:

1. **Protein** - Protein.
2. **PBD** - Protein 3d.
3. **Url** - User files.
"""

from django.db import models
from django.utils import timezone

# === Protein ===
class Protein(models.Model):
    entry = models.CharField(max_length=50, primary_key=True, unique=True)
    entry_name = models.CharField(max_length=50, unique=True)
    protein_names = models.CharField(max_length=800)
    cross_reference_GeneID = models.CharField(max_length=20)
    gene_names = models.CharField(max_length=200)
    organism = models.CharField(max_length=80)
    taxonomy_lineage = models.CharField(max_length=300)
    sequence = models.TextField()
    length = models.PositiveIntegerField()
    created_date = models.DateTimeField(default=timezone.now)
    cross_reference_PDB = models.TextField()

    class Meta:
        """
        Order protein model
        """
        ordering = ['entry']

    def __str__(self):
        return '%s %s' % (self.entry, self.entry_name)

# === PBD ===
class PBD(models.Model):
    id_pbd = models.CharField(max_length=100, primary_key=True, unique=True)
    entry = models.CharField(max_length=200)
    protein = models.ForeignKey(Protein, on_delete=models.CASCADE)
    description = models.CharField(max_length=300)
    created = models.DateField(default=timezone.now)

    def __str__(self):
        return '%s' % (self.id_pbd)

    class Meta:
        """
        Order pbd model
        """
        ordering = ('created',)


def user_directory_path(instance, filename):
    """
    File will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    """
    return 'media/'+'user_{0}/{1}'.format(instance.user.id, filename)

# === Url ===
class Url(models.Model):
    title = models.CharField(max_length=100)
    types = models.CharField(max_length=100, blank=True, null=True)
    user = models.CharField(max_length=100)
    gb = models.FileField(upload_to=user_directory_path, unique=True)
    sequences = models.TextField(blank=True, null=True)
    created_date = models.DateField(default=timezone.now)

    def __str__(self):
        return '%s' % (self.title)

    class Meta:
        """
        Order url model
        """
        ordering = ('created_date',)

    def delete(self, *args, **kwargs):
        """
        Delete file
        """
        self.gb.delete()
        super().delete(*args, **kwargs)

    def display_text_file(self):
        """
        Add a blank space to the beginning of the file
        """
        originalContent = None
        with open(self.gb.path, 'r+') as filep:
            originalContent = filep.read()

        with open(self.gb.path, 'w') as filep:
            filep.seek(0, 0)
            filep.write('\n')
            filep.write(originalContent)

        with open(self.gb.path, 'r+') as filep:
            return filep.read()
