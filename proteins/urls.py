"""
Urls of the application.
"""

from django.urls import path
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url
from django_filters.views import object_filter
from . import views


urlpatterns = [
    path('', views.ProteinListView.as_view(), name='protein_list'),
    path('list', views.pbd_list , name='pbd_list'),
    path('urls', views.urls_list, name='urls_list'),
    path('urls/upload', views.urls_upload, name='urls_upload'),
    path('urls/<int:pk>', views.delete_url, name='delete_url'),
    path('urls/all', views.delete_all_urls, name='delete_all_urls'),
    path('register', views.register, name='register'),
    path('login', views.login , name='login'),
    path('logout', views.logout, name='logout'),
    path('searchncbi', views.search_term_ncbi, name='search_term_ncbi'),
    path('align', views.align_sequence, name='align_sequence'),
    path('align/multiple', views.align_multiple_sequence, name='align_multiple_sequence'),
    path('window/<int:pk>', views.read_file, name='read_file'),
    path('urls/multiple/<gb>', views.align_multiple_list, name='align_multiple_list'),
    path('help/admin', views.help_admin, name='help_admin'),
    path('help/users', views.help_user, name='help_user'),
    #path('align/tree', views.align_phylogenetic_tree, name='phylogenetic_tree'),
]



if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
