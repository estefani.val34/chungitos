"""
Apps
"""
from django.apps import AppConfig


class ProteinsConfig(AppConfig):
    """
    Name App.
    It is added in settings of mysiteproyect.
    """
    name = 'proteins'
