"""
Forms that are used in views.py.
Contains 4 forms:

1. **NCBIForm** - Form used in function search_term_ncbi of views.py
2. **UrlForm** - Form used in function urls_upload of views.py.
3. **AlighnmentForm** - Form used in function align_sequence of views.py.
4. **AlighnmentMultipleForm** - Form used in function align_multiple_sequence of views.py.
"""

from django import forms
from django.forms import ModelForm
from django.utils.translation import gettext_lazy as _
from django.core.validators import RegexValidator
# defined in [[models.py]]
from .models import Url

CHOICES = [('1', 'global_Needleman_Wunsch'), ('2', 'local_Smith_Waterman')]

# === NCBIForm ===
class NCBIForm(forms.Form):
    """
    Form used in function search_term_ncbi of views.py.
    Create 3 inputs. 
    """
    term = forms.CharField(label='Term', max_length=100, required=True)
    num_downloads = forms.IntegerField(label='Number of downloads', required=True, min_value=1)
    database = forms.CharField(max_length=100, required=True)


# === UrlForm ===
class UrlForm(forms.ModelForm):
    """
    Form used in function urls_upload of views.py.
    Create two fields ('title', 'gb') according to the characteristics of the Url model.
    """
    class Meta:
        """
        Call Url model
        """
        model = Url
        fields = ('title', 'gb')

        labels = {
            'gb': _('File'),
        }


# === AlighnmentForm ===
class AlighnmentForm(forms.Form):
    """
    Form used in function align_sequence of views.py.
    Create 3 fields.
    """
    secuencia1 = forms.CharField(widget=forms.Textarea, required=True, validators=[
        RegexValidator(
            regex='^[a-zA-Z\s]+$',
            message='Secuencia 1 must be letters',
            code='invalid_secuencia1'
        ),
    ])
    secuencia2 = forms.CharField(widget=forms.Textarea, required=True, validators=[
        RegexValidator(
            regex='^[a-zA-Z\s]+$',
            message='Secuencia 2 must be letters',
            code='invalid_secuencia2'
        ),
    ])
    align = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICES, required=True)


# === AlighnmentMultipleForm ===
class AlighnmentMultipleForm(forms.ModelForm):
    """
    Form used in function align_multiple_sequence of views.py.
    Create two fields ('sequences', 'gb',) according to the characteristics of the Url model.
    """

    gb = forms.FileField(required=False, label='File')

    class Meta:
        """
        Call Url model
        """
        model = Url
        fields = ('sequences', 'gb',)

        labels = {
            'gb': _('File'),
        }

# === Treeform. By Paco ===
class Treeform(forms.Form):
    """
    Form used in function search_term_ncbi of views.py.
    Create 3 inputs. 
    """
    mfasta = forms.FileField(required=False, label='File')

