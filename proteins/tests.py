"""
Test
Contains :

1. **MySeleniumTests** - Automatic login test (1 test).
2. **SimpleTest** - Client Test (3 tests).
3. **UrlTestCase** - Deleted Urls (1 test).


References:

Django: https://docs.djangoproject.com/en/3.0/topics/testing/tools/

Automatic Test : https://www.selenium.dev/

Command Run Tests : ./manage.py test proteins.tests

Status Codes: https://www.django-rest-framework.org/api-guide/status-codes/
"""
import pytest
from django.core.management import call_command
from django.test import TestCase
from django.test import Client
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# defined in [[models.py]]
from proteins.models import Url


# === MySeleniumTests ===
class MySeleniumTests(StaticLiveServerTestCase):
    """
    Automatically open Firefox then go to the login page, enter the
    credentials and press the “Log in” button.
    """
  
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_login(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/login'))
        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys('myuser')
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('secret')
        self.selenium.find_element_by_xpath('//input[@value="Log in"]').click()


# === SimpleTest ===
class SimpleTest(TestCase):
    """
    Client Test
    """
    def test_list(self):
        response = self.client.get('/list')
        self.assertEqual(response.status_code, 200)

    def test_urls(self):
        response = self.client.get('/urls')
        self.assertEqual(response.status_code, 200)

    def test_multiple_fasta(self):
        c = Client()
        response = None
        with open('filesTests/opuntia.fasta') as fp:
            response = c.post('/align/multiple', {'gb': fp})
        self.assertEqual(response.status_code , 200)


# === UrlTestCase ===
class UrlTestCase(TestCase):
    """
    Test if all are deleted Urls
    """
    def setUp(self):
        self.test_delete()

    def test_delete(self):
        Url.objects.all().delete()
        self.assertQuerysetEqual(Url.objects.all(),[])
