
"""
Views where functions and templates are called.
Currently we support the following 15 functions:

1.  **Read_file** - Open window
2.  **Search_term_ncbi** - Manage the Search Term section of the menu
3.  **Urls_list** - Manage the User History section of the menu
4.  **Urls_upload** - Manage the Upload File section of the menu
5.  **Delete_url** - Delete file
6.  **Delete_all_urls** - Delete files of user directory
7.  **Align_multiple_list** - Show alignment files
8.  **Align_multiple_sequence** - Manage the Multiple Alignment section of the menu
9.  **Align_sequence** - Manage the Alignment Two Sequences section of the menu
10. **ProteinListView** - Shows the list of all proteins
11. **Logout** - Logout user
12. **Register** - Register
13. **Login** - Login
14. **Help_admin** - HTML manual admin
15. **Help_user** - HTML manual users
"""
import re
import os
from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth import logout as do_logout
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login as do_login
from django.contrib.auth.forms import UserCreationForm
from django.views.generic import ListView, DetailView
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse


# defined in [[phylo.index.py]]
#from phylo.index import make_tree, handle_uploaded_file, from_multifasta_to_phylip

# defined in [[forms.py]]
from .forms import (
    NCBIForm,
    UrlForm,
    AlighnmentForm,
    AlighnmentMultipleForm)

# defined in [[models.py]]
from .models import Protein, PBD, Url

# defined in [[filters.py]]
from .filters import PbdFilter

# defined in [[entrez_ncbi.py]]
from .entrez_ncbi import (
    search_something,
    search_something_structure)

# defined in [[alighnw.py]]
from align.alighnw import align_nw

# defined in [[alighsw.py]]
from align.alighsw import align_sw

# defined in [[multialignfasta.py]]
from align.multialignfasta import (
    generate_aln_dnd,
    write_file_sequences,
    delete_dir,
    validate_file,
    is_fasta_extension,
    len_seq_ids, empty_sequences)

# from django.contrib.auth.models import User


# === Read_file ===
def read_file(request, pk):
    """
    Open window when user click icon glyphicon-new-window

    Used in the templates : urls_list , align_multiple_list

    Name of the related URL : read_file

    Arguments:

      request {request} -- request

      pk {int} -- pk of url object

    Returns:

      {} -- open a new window with the content of the file
    """
    error = ""
    obj = Url.objects.get(pk=pk)
    path = str(obj.gb)
    try:
        f = open(path, 'r')
        file_content = f.read()
        f.close()
        return HttpResponse(file_content, content_type="text/plain")
    except Exception as e:
        error = "Can't open this file."
        return HttpResponse(error)


# === Search_term_ncbi ===
def search_term_ncbi(request):
    """
    Manage the Search Term section of the menu.

    Used in the templates : term_ncbi_search

    Name of the related URL : search_term_ncbi

    Arguments:

      request {} -- request

    Returns:

      {} -- show template

    """

    form = NCBIForm()
    uploaded_file = []
    uploaded_file_structure = []
    list_media_user = []
    if request.method == "POST":
        form = NCBIForm(data=request.POST)
        if form.is_valid():
            try:
                term = form.cleaned_data['term']
                num = form.cleaned_data['num_downloads']
                database = form.cleaned_data['database']
                user = "user_"+str(request.user.id)
                user_username = str(request.user)
                if database == 'structure':
                    uploaded_file_structure = search_something_structure(
                        term, num, user, database)
                else:
                    uploaded_file = search_something(
                        term, num, user, user_username, database)
            except Exception as e:
                error = "The page could not be loaded, please try again later."
                return HttpResponse(error)

    return render(request, 'proteins/term_ncbi_search.html',
                  {'form': form,  'urls': uploaded_file, 'uploaded_file_structure': uploaded_file_structure})

# === Urls_list ===
def urls_list(request):
    """
    Manage the User History section of the menu

    Used in the templates : urls_list

    Name of the related URL : urls_list

    Arguments:

      request {} -- request

    Returns:

      {} -- template
    """

    user = str(request.user)
    urls = Url.objects.all().filter(user=user)
    return render(request, 'proteins/urls_list.html', {
        'urls': urls
    })

# === Urls_upload ===
def urls_upload(request):
    """
    Manage the Upload File section of the menu

    Used in the templates : urls_upload

    Name of the related URL : urls_upload

    Arguments:

      request {} -- request

    Returns:

      {} -- template
    """

    try:
        if request.method == 'POST':
            form = UrlForm(request.POST, request.FILES)
            if form.is_valid():
                post = form.save(commit=False)
                post.user = request.user
                post.sequences = None
                post.save()
                return redirect('urls_list')
        else:
            form = UrlForm()
        return render(request, 'proteins/urls_upload.html', {
            'form': form
        })
    except Exception as e:
        error = "An error has occurred."
        return HttpResponse(error)


# === Pbd_list ===
def pbd_list(request):
    """
    Manage the Protein 3D section of the menu

    Used in the templates : pbd_list

    Name of the related URL : pbd_list

    Arguments:

      request {} -- request

    Returns:

      {} -- template
    """
    protein_list = PBD.objects.all()
    f = PbdFilter(request.GET, queryset=protein_list)

    paginator = Paginator(f.qs, 7)
    page = request.GET.get('page')

    try:
        proteins = paginator.page(page)
    except PageNotAnInteger:
        proteins = paginator.page(1)
    except EmptyPage:
        proteins = paginator.page(paginator.num_pages)

    return render(request, 'proteins/pbd_list.html', {
        'proteins': proteins,
        'page': page,
        'filter': f,
    })

# === Delete_url ===
def delete_url(request, pk):
    """
    Delete file (of Url model and user directory) when user click icon glyphicon-trash

    Used in the templates : urls_list, align_multiple_list

    Name of the related URL : delete_url

    Arguments:

      request {} -- request
      pk 

    Returns:

      {} -- delete file
    """
    if request.method == 'POST':
        url = Url.objects.get(pk=pk, user=request.user)
        url.delete()
    path_refered = str(request.META.get('HTTP_REFERER'))
    return redirect(path_refered)

# === Delete_all_urls ===
def delete_all_urls(request):
    """
    Delete files of user directory when user click button Delete All

    Used in the templates : urls_list

    Name of the related URL : delete_all_urls

    Arguments:

      request {} -- request

    Returns:

      {} -- delete directory
    """
    if request.method == 'POST':
        url = Url.objects.all().filter(user=request.user)
        url.delete()
        delete_dir('media/user_'+str(request.user.id))
    path_refered = str(request.META.get('HTTP_REFERER'))
    return redirect(path_refered)


# === Align_multiple_list ===
def align_multiple_list(request, gb):
    """
    Show alignment files when user click button 
    Align of Multiple Sequence Alignment section of menu

    Used in the templates : align_multiple_list

    Name of the related URL : align_multiple_list

    Arguments:

      request {} -- request

    Returns:

      {} -- template
    """
    urls = Url.objects.all().filter(gb__contains=gb, user=request.user)

    return render(request, 'proteins/align_multiple_list.html', {
        'urls': urls, 'gb': gb
    })

# === Align_multiple_sequence ===


def align_multiple_sequence(request):
    """
    Manage the Multiple Alignment section of the menu

    Call functions : align_multiple_list

    Used in the templates : align_multiple_sequence

    Name of the related URL : align_multiple_sequence

    Arguments:

      request {} -- request

    Returns:

      {} -- template
    """
    errornogb = ""
    errorseq = " "
    errorfile = ""
    errorlen = ""
    if request.method == 'POST':
        form = AlighnmentMultipleForm(request.POST, request.FILES)
        seq = request.POST.get('sequences', '')
        gb = request.FILES.get('gb', '')

        if seq == "" or seq == None:
            if gb == "" or gb == None:
                errornogb = "Please enter a File or Sequences."
            else:
                if is_fasta_extension(str(gb)) == True:
                    if form.is_valid():
                        fil = request.FILES['gb']
                        sequences = form.cleaned_data['sequences']
                        post = form.save(commit=False)
                        post.title = str(fil)
                        post.types = 'MultiFasta'
                        post.user = request.user
                        post.save()

                        if empty_sequences(str(post.gb)) == True:
                            if validate_file(str(post.gb)) == True:
                                tup_files = generate_aln_dnd(
                                    str(post.gb), request.user)
                                return redirect('align_multiple_list', gb=tup_files[1]+'.')
                            else:
                                errorseq = "The number of sequences must be greater than or equal to two, the name of the sequences must be unique"
                        else:
                            errorlen = "A sequence can not be empty, sequences can only be letters, sequences names can not  be empty"
                else:
                    errorfile = "The file must be in fasta format."
        else:
            if gb == "" or gb == None:
                path_gb = write_file_sequences(seq, request.user)
                if empty_sequences(str(path_gb)) == True:
                    if validate_file(str(path_gb)) == True:
                        tup_files = generate_aln_dnd(
                            str(path_gb), request.user)
                        return redirect('align_multiple_list', gb=tup_files[1])
                    else:
                        errorseq = "The number of sequences must be greater than or equal to two, the name of the sequences must be unique"
                else:
                    errorlen = "A sequence can not be empty, sequences can only be letters, sequences names can not  be empty"
            else:
                errornogb = "Please enter a File or Sequences."

    else:
        form = AlighnmentMultipleForm()
    return render(request, 'proteins/align_multiple_sequence.html', {'form': form,
                                                                     'errornogb': errornogb, 'errorseq': errorseq, 'errorfile': errorfile, 'errorlen': errorlen})


# === Align_sequence ===
def align_sequence(request):
    """
    Manage the Alignment Two Sequences section of the menu

    Used in the templates : align_sequence

    Name of the related URL : align_sequence

    Arguments:

      request {} -- request

    Returns:

      {} -- template
    """

    form = AlighnmentForm()
    alignment = []
    alignment_1_names = []
    alignment_2_traceback = []
    alignment_3_matrix = []
    match = ""
    gap = ""
    if request.method == 'POST':
        form = AlighnmentForm(request.POST)
        if form.is_valid():
            secuencia1 = form.cleaned_data['secuencia1']
            secuencia2 = form.cleaned_data['secuencia2']
            align = form.cleaned_data['align']
            #delete blank spaces
            lseq1 = re.findall('[A-Za-z]', secuencia1) 
            lseq2 = re.findall('[A-Za-z]', secuencia2) 
            secu1 =  "".join(lseq1)
            secu2 =  "".join(lseq2)
 
            if align == "1":
                match = 1
                gap = 1
                alignment = align_nw(secu1, secu2)
                alignment_1_names = alignment[0:2]
                alignment_2_traceback = alignment[2]
                alignment_3_matrix = alignment[3]

            else:
                alignment = align_sw(secu1, secu2, 3, 2)
                if alignment[0] != "There is no similarity.":
                    match = 3
                    gap = 2
                    alignment_1_names = alignment[0:2]
                    alignment_2_traceback = alignment[2]
                    alignment_3_matrix = alignment[3]
                else:
                    return render(request, 'proteins/align_sequence.html', {'form': form, 'alignment': alignment,   'alignment_1': alignment_1_names,
                                                                            'alignment_2': alignment_2_traceback, 'alignment_3': alignment_3_matrix,
                                                                            'match': match, 'gap': gap})

    return render(request, 'proteins/align_sequence.html', {'form': form, 'alignment_1': alignment_1_names,
                                                            'alignment_2': alignment_2_traceback, 'alignment_3': alignment_3_matrix,
                                                            'match': match, 'gap': gap})


# === ProteinListView ===
class ProteinListView(ListView):
    """
    Shows the list of all proteins

    Used in the templates : protein_list

    Name of the related URL : protein_list

    Arguments:

      ListView {} -- type of view 
    """

    model = Protein
    template_name = 'proteins/protein_list.html'
    context_object_name = 'proteins'
    paginate_by = 6
    queryset = Protein.objects.all()


# === Logout ===
def logout(request):
    """
    Logout user. Return main page. 

    Name of the related URL : logout

    Arguments:

      request {} -- request

    Returns:

      {} -- main page
    """

    do_logout(request)
    return redirect('/')


# === Register ===
def register(request):
    """
    Register

    Used in the templates : register

    Name of the related URL : register

    Arguments:

      request {} -- request

    Returns:

      {} -- template
    """
    form = UserCreationForm()
    if request.method == "POST":
        form = UserCreationForm(data=request.POST)
        if form.is_valid():
            user = form.save()
            if user is not None:
                do_login(request, user)
                return redirect('/')
    form.fields['username'].help_text = None
    form.fields['password1'].help_text = None
    form.fields['password2'].help_text = None

    return render(request, "proteins/register.html", {'form': form})

# === Login ===
def login(request):
    """
    Login 

    Used in the templates : login

    Name of the related URL : login

    Arguments:

      request {} -- request

    Returns:

      {} -- template
    """
    form = AuthenticationForm()
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                do_login(request, user)
                return redirect('/')
    return render(request, "proteins/login.html", {'form': form})

# === Help_admin ===
def help_admin(request):
    """
    Show manual admin when user click link Help Admin

    Used in the templates : manualAdmin

    Name of the related URL :  help_admin

    Arguments:

      request {} -- request

    Returns:

      {} -- template
    """

    return render(request, "manuals/manualAdmin.html")

# === Help_user ===
def help_user(request):
    """
    Show manual users when user click link Help User

    Used in the templates : manualUsers

    Name of the related URL : help_user

    Arguments:

      request {} -- request

    Returns:

      {} -- template
    """
    return render(request, "manuals/manualUsers.html")


# def align_phylogenetic_tree(request):
#     form = Treeform()
#     if request.method == 'POST':
#         print(request.FILES['mfasta'])
#         form = Treeform(request.POST, request.FILES)
#         if form.is_valid():
#           handle_uploaded_file(request.FILES['mfasta'])
#           make_tree('media/'+str(request.FILES['mfasta']))
#           return HttpResponse('Se ha descargado el fichero')
#     else:
#         form = Treeform()

#     return render(request, "proteins/phylogenetic_tree.html", {'form': form})