"""
Filters that are used in views.py.
Contains 1 filter:

1. **PbdFilter** - Filter PBD by protein.
"""
import django_filters
# defined in [[models.py]]
from .models import PBD

# === PbdFilter ===
class PbdFilter(django_filters.FilterSet):
    """
    Filter used in function pbd_list of views.py
    """
    class Meta:
        """
        Call PBD model and define filters
        """
        model = PBD
        fields = {
            'protein': ['exact'],
            'entry':['icontains'],
        }
