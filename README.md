# Chungitos Project

[![Python Version](https://img.shields.io/badge/python-3.8.2-brightgreen.svg)](https://python.org)
[![Django Version](https://img.shields.io/badge/django-3.0.6-brightgreen.svg)](https://djangoproject.com)

End of course project, done by:

    - Estefani Paredes Valera

## 1. A description of each directory in the source code

```bash
mysiteproyect/
    align/
        alighnw_lib.py
        alighnw.py
        alighsw_lib.py
        alighsw.py
        multialignfasta.py

    docs

    filesTest/
        opuntia.fasta

    infiles/
        P27348.txt
        ...

    media/
        user_1/
            opuntia.fasta
            opuntia.aln
            ...
        user_2
        ...

    mysiteproyect/
        __init__.py
        asgi.py
        settings.py
        urls.py
        wsgi.py

    proteins/
        migrations/
            __init__.py
        static/
            css/
                protein.css
        templates/
            manuals/
                images
                manualAdmin.html
                manualUsers.html
            proteins
        __init__.py
        admin.py
        apps.py
        entrez_ncbi.py
        filters.py
        forms.py
        models.py
        tests.py
        urls.py
        views.py
    
    .python-version
    clustalw
    db.sqlite3
    geckodriver.log
    manage.py
    readFieldsUniprotList.py

```
These files are:
-   The outer **mysiteproyect** root directory is a container for our project.
-   The **align** directory contains the necessary data to make the alignment of two sequences.
-   The file **alighnw_lib.py** where there are the functions to do the Needleman Wunsch alignment. [Doc](docs/alighnw_lib.html) and [File](align/alighnw_lib.py)
-   The file **alighnw.py** calls the functions of the alighnw_lib.py file. [Doc](docs/alighnw.html) and [File](align/alighnw.py)
-   The file **alighsw_lib.py** where there are the functions to do the local Smith Waterman alignment. [Doc](docs/alighsw_lib.html) and [File](align/alighsw_lib.py)
-   The file **alighsw.py** calls the functions of the alighsw_lib.py file. [Doc](docs/alighsw.html) and [File](align/alighsw.py)
-   The file **multialignfasta.py** where there are the functions to do multiple alignment whith clustal. [Doc](docs/multialignfasta.html) and [File](align/multialignfasta.py)
-   The **docs** directory contains html files. They have been created with Pycco [Generating Code Documentation with Pycco](https://realpython.com/generating-code-documentation-with-pycco/)
-   The **filesTest** directory contains an example of fasta file to **tests.py**.
-   The **infiles** directory contains files, in .txt format, that we have downloaded from the Uniprot database. We have downloaded these files to extract the information that interests us and to be able to save this information in our own database. Downloading the files and extracting the information is done by the file **readFieldsUniprotList.py**.
-   The **media** directory contains the files of each user. All the files that the user uses in our application are saved in their respective folder.
-   The **mysiteproyect** directory contains the configuration files of our project. 
-   **mysiteproyect/ __init__.py** is an empty file that tells Python that this directory should be considered a Python package. [Read more about packages](https://docs.python.org/3/tutorial/modules.html#tut-packages). [File](mysiteproyect/__init__.py)
-   **mysiteproyect/asgi.py** is an entry-point for ASGI-compatible web servers to serve your project. See [How to deploy with ASGI](https://docs.djangoproject.com/en/3.0/howto/deployment/asgi/) for more details. [File](mysiteproyect/asgi.py)
-   **mysiteproyect/settings.py** where are Settings/configuration for this Django project. [Django settings](https://docs.djangoproject.com/en/3.0/topics/settings/) will tell you all about how settings work. [File](mysiteproyect/settings.py)
-   **mysiteproyect/urls.py** where are the URL declarations for this Django project; a “table of contents” of your Django-powered site. You can read more about URLs in [URL dispatcher](https://docs.djangoproject.com/en/3.0/topics/http/urls/). [File](mysiteproyect/urls.py)
-   **mysiteproyect/wsgi.py** is an entry-point for WSGI-compatible web servers to serve your project. See [How to deploy with WSGI](https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/) for more details. [File](mysiteproyect/wsgi.py)
-   The **proteins/** directory is where our application is located.
-   In the **proteins/migrations/** directory where the migrations of our app are saved. Migrations are the changes that are made in the models (and therefore in the database).
-   **proteins/static/css/** where are the style files.
-   **proteins/templates/manuals/** contains the help templates. 
-   **proteins/proteins/** is a directory containing the templates used in **views.py**.
-   **proteins/__init__.py** is an empty file that tells Python that this directory should be considered a Python package. [Read more about packages](https://docs.python.org/3/tutorial/modules.html#tut-packages).
-   **proteins/admin.py** where the administrator url is configured. Generating admin sites for your staff or clients to add, change, and delete content is tedious work that doesn’t require much creativity. For that reason, Django entirely automates creation of admin interfaces for models. It reads metadata from your models to provide a quick, model-centric interface where trusted users can manage content on your site. [File](proteins/admin.py) and [More info](https://docs.djangoproject.com/en/3.0/intro/tutorial02/#introducing-the-django-admin)
-   **proteins/apps.py** where is the name of our app so that it can be called from **mysiteproyect/settings.py**. [File](proteins/apps.py)
-   **proteins/entrez_ncbi.py** contains functions used in the function search_term_ncbi of **views.py**. [Doc](docs/entrez_ncbi.html) and [File](proteins/entrez_ncbi.py)
-   **proteins/filters.py** contains filter used in function pbd_list of **views.py**. [Doc](docs/filters.html) and [File](proteins/filters.py)
-   **proteins/forms.py** contains forms that are used in **views.py**. Django provides a rich framework to facilitate the creation of forms and the manipulation of form data. [Doc](docs/forms.html) and [File](proteins/forms.py)
-   **proteins/models.py** contains models to create the tables in the database. Django provides an abstraction layer (the “models”) for structuring and manipulating the data of your Web application. [Doc](docs/models.html) and [File](proteins/models.py)
-   **proteins/tests.py** contain the app tests. [Doc](docs/tests.html) and [File](proteins/tests.py)
-   **proteins/urls.py** contains the urls of the application. It is where the functions that are in views are called. [Doc](docs/urls.html) and [File](proteins/urls.py)
-   **proteins/views.py** is where functions and templates are called. Django has the concept of “views” to encapsulate the logic responsible for processing a user’s request and for returning the response. [Doc](docs/views.html) and [File](proteins/views.py)
-   **.python-version** indicates the python version with which we have made the application.
-   **clustalw** is the executable that allows us to do multiple alignment.
-   **db.sqlite3** is the default database generated by Django.
-   **geckodriver.log** is a Software testing. It is used in **test.py** [What is GeckoDriver? Selenium](https://www.linkedin.com/pulse/what-geckodriver-amith-wijesiri)
-   **manage.py** is a command-line utility that lets you interact with this Django project in various ways. You can read all the details about manage.py in [django-admin and manage.py](https://docs.djangoproject.com/en/3.0/ref/django-admin/).
-   **readFieldsUniprotList.py** fill the database tables with Uniprot values. This file is only called once. [Doc](docs/readFieldsUniprotList.html) and [File](readFieldsUniprotList.py).


## 2. The location of the main() function in your program.
```bash
proteins/
        migrations/
            __init__.py
        static/
            css/
                protein.css
        templates/
            manuals/
                images
                manualAdmin.html
                manualUsers.html
            proteins
        __init__.py
        admin.py
        apps.py
        entrez_ncbi.py
        filters.py
        forms.py
        models.py
        tests.py
        urls.py
        views.py
```
-   The functionalities of our application are found in the **proteins/** folder.
-   The functions are in **views.py**. [File](proteins/views.py)


## 3. Running the Project Locally.

The essential first step is to [install python 3.8.2](https://linuxize.com/post/how-to-install-python-3-8-on-ubuntu-18-04/). We work in Ubuntu 18.04, the commands are:

```bash
sudo apt update
sudo apt install software-properties-common
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt install python3.8
python3.8 --version
```

Now, clone the repository to your local machine:

```bash
git clone https://gitlab.com/estefani.val34/chungitos.git
```

Install the requirements (you need to have installed the pip):

```bash
cd chungitos/
python -m pip install Django
pip install django-filter
pip install django-crispy-forms
pip install django-bootstrap-form
pip install biopython
python -m pip install selenium
python3 -m pip install requests
pip3 install pytest
pip install pysqlite3
```

Apply the migrations:

```bash
python manage.py migrate
```

Finally, run the development server:

```bash
python manage.py runserver
```

The project will be available at **127.0.0.1:8000**.

>Note:

>*If there are no values ​​in the database tables, do this:*

>*Fill the tables of the database:*
>```bash
>python readFieldsUniprotList.py
>```

>*Reapply migrations:*
>```bash
>python manage.py migrate
>```
>*Do not do if the database already has value, to avoid unique errors*



```