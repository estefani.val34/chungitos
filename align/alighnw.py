"""
Program which recreates the Needleman Wunsch alignment.
Currently we support the following 1 function:

1. **Align_nw** - Call the functions of alighnw_lib (jump to section in  [[alighnw_lib.py]] )
"""
# defined in [[alighnw_lib.py]]
from align.alighnw_lib import get_matrix, get_alignment


# === Align_nw ===
def align_nw(secuencia_1, secuencia_2):
    """
    Call the functions of alighnw_lib

    Arguments:

      secuencia_1 {[str]} -- Sequence one

      secuencia_2 {[str]} -- Sequence two

    Returns:

      [list] -- The list contains the sequences, the alignment and the matrix

    """
    result = []
    matr = get_matrix(secuencia_1, secuencia_2)
    result = [secuencia_1.upper(), secuencia_2.upper(
    ), get_alignment(matr, secuencia_1, secuencia_2), matr]

    return result
