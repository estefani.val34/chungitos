"""
Program which recreates the local Smith Waterman alignment.
Currently we support the following 5 functions:

1. **Get_similarity** - Returns if two letters are equal
2. **Get_score** - Return  score
3. **Traceback** - Get the alignment of the sequences
4. **Create_matrix** - Create matrix of alignment
5. **Smith_waterman_algorithm** - Call the others functions
"""

import numpy as np

# === Get_similarity ===
def get_similarity(a_letter: str, b_letter: str, match: int):
    """
    Returns if two letters are equal

    Arguments:

      a_letter {str} -- first letter

      b_letter {str} -- second letter

      match {int} -- the punctuation if the letters are the same

    Returns:

      [int] -- the punctuation
    """

    if a_letter == b_letter:
        return match
    return -match

# === Get_score ===
def get_score(adn1: str, adn2: str, matriz: np.ndarray):
    """
    Give the matrix score

    Arguments:

      adn1 {str} -- sequence one

      adn2 {str} -- sequence two

      matriz {np.ndarray} -- matrix

    Returns:

      [tuple] -- position
    """

    seq1 = len(adn1) + 1
    seq2 = len(adn2) + 1
    max = 0
    position = ""
    for i in range(seq1):
        for j in range(seq2):
            if max < matriz[i][j]:
                max = matriz[i][j]
                position = (str(i), str(j))
    return position


# === Traceback ===
def traceback(adn1: str, adn2: str, max: str, matriz: np.ndarray, match: int, gap: int):
    """
    Get the alignment of the sequences

    Arguments:

      adn1 {str} -- sequence one

      adn2 {str} -- sequence two

      max {str} --  position max

      matriz {np.ndarray} -- matrix

      match {int} -- punctuation if there is match

      gap {int} -- punctuation if there is a gap

    Returns:

      [list] -- alignment of the sequences
    """

    result = []
    aligment_a = ""
    aligment_a_list = []
    aligment_b = ""
    aligment_b_list = []
    aligment_c = ""
    aligment_c_list = []

    i = int(max[0])
    j = int(max[1])
    while(i > 0 and j > 0):
        similarity = get_similarity(adn1[i-1], adn2[j-1], match)
        if i > 0 and j > 0 and matriz[i][j] == matriz[i-1][j-1] + similarity:
            aligment_a += adn1[i-1]
            aligment_a_list.append(adn1[i-1])
            aligment_b += adn2[j-1]
            aligment_b_list.append(adn2[j-1])
            aligment_c += "|"
            aligment_c_list.append("|")

            i = i-1
            j = j-1
        elif(i > 0 and matriz[i][j] == matriz[i-1][j]+(-gap)):
            aligment_a += adn1[i-1]
            aligment_a_list.append(adn1[i-1])
            aligment_b += "-"
            aligment_b_list.append("-")
            aligment_c += " "
            aligment_c_list.append(" ")
            i = i-1
        else:
            aligment_a += "-"
            aligment_a_list.append("-")
            aligment_b += adn2[j-1]
            aligment_b_list.append(adn2[j-1])
            aligment_c += " "
            aligment_c_list.append(" ")

            j = j-1

    result = [aligment_a_list[::-1],
              aligment_c_list[::-1], aligment_b_list[::-1]]
    return result

# === Create_matrix ===
def create_matrix(adn1: str, adn2: str, match: int, gap: int):
    """
    Create matrix

    Arguments:

      adn1 {str} -- sequence one

      adn2 {str} -- sequence two

      match {int} -- punctuation if there is match

      gap {int} -- punctuation if there is a gap

    Returns:

      [np.ndarray] -- matrix of alignment
    """

    len_adn1 = len(adn1)
    len_adn2 = len(adn2)
    mat = np.zeros((len_adn1+1, len_adn2+1), int)
    for i in range(len_adn1):
        for j in range(len_adn2):
            diagonal = mat[i][j]
            abajo = mat[i+1][j]
            derecha = mat[i][j+1]
            if adn1[i] == adn2[j]:
                mat[i+1][j+1] = max(diagonal+match, abajo-gap, derecha-gap, 0)
            else:
                mat[i+1][j+1] = max(diagonal-match, abajo-gap, derecha-gap, 0)
    return mat


# === Smith_waterman_algorithm ===
def smith_waterman_algorithm(adn1: str, adn2: str, match: int, gap: int):
    """
    Call the others functions

    Arguments:

      adn1 {str} -- sequence one

      adn2 {str} -- sequence two

      match {int} -- punctuation if there is match

      gap {int} --  punctuation if there is gap

    Returns:

      [list] -- Content the others functions
    """

    result = []
    adn1 = adn1.upper()
    adn2 = adn2.upper()
    matriz = create_matrix(adn1, adn2, 3, 2)

    if get_score(adn1, adn2, matriz) == "":
        result = ["There is no similarity."]
    else:
        postion_max = get_score(adn1, adn2, matriz)

        traceback(adn1, adn2, postion_max, matriz, match, gap)

        result = [adn1, adn2, traceback(
            adn1, adn2, postion_max, matriz, match, gap), matriz]

    return result
