"""
Program which recreates local Smith Waterman alignment.
Currently we support the following 1 function:

1. **Align_sw** - Call the functions of alighsw_lib (jump to section in  [[alighsw_lib.py]] )
"""

# defined in [[alighsw_lib.py]]
from align.alighsw_lib import smith_waterman_algorithm

# === Align_sw ===
def align_sw(secuencia_1, secuencia_2, match, gap):
    """
    Call the functions of alighsw_lib

    Arguments:

      secuencia_1 {[type]} -- sequence one

      secuencia_2 {[type]} -- sequence two

      match {[type]} -- punctuation if there is match

      gap {[type]} -- punctuation if there is gap

    Returns:

      [list] -- smith waterman algorithm
    """

    return smith_waterman_algorithm(secuencia_1, secuencia_2, match, gap)
