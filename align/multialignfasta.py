"""
Multiple alignment whith Clustal.
These functions are used in [[views.py]] in the function align_multiple_sequence().

Currently we support the following 16 functions:

1.  **Generate_aln_dnd** - Generate files (aln, dnd) of alignment
2.  **Get_names_aln_dnd** - Give paths of the files aln and dnd
3.  **Get_title_file** - Give the names of the files
4.  **Get_name_title_file** - Gives the name of a file, without the extension .dnd
5.  **Random_string** - Generate random names unique
6.  **Write_file_sequences** - Create a file when the user enters sequences
7.  **Delete_dir** - Delete all files of directory ( used in the function delete_all_urls())
8.  **Validate_file** - Validate sequences
9.  **Is_fasta** - Format .fasta
10. **Is_fasta_extension** - Check if the file extension is .fasta, .fa, .fna, .ffn, .faa, .frn
11. **Empty_sequences** - Check if sequences are empty
12. **Len_seq_ids** - Check if the sequences are correct
13. **Names_empty** - Value empty in a list
14. **is_blanck** - String empty
15. **Num_seq** - Return the number of sequences
16. **Unique_values** - The content after the character  > must be unique


References:

Multiple sequence alignment :
https://en.wikipedia.org/wiki/Multiple_sequence_alignment

The effect of the guide tree on multiple sequence alignments and subsequent phylogenetic analyses :
https://www.ncbi.nlm.nih.gov/pubmed/18229674

Biopython :
http://biopython.org/DIST/docs/tutorial/Tutorial.html#htoc82

FASTA file :
https://en.wikipedia.org/wiki/FASTA_format
"""


import os
import re
import shutil
import random
import string
from Bio import SeqIO
from Bio.Align.Applications import ClustalwCommandline
# defined in [[models.py]]
from proteins.models import Url

# === Generate_aln_dnd ===
def generate_aln_dnd(file_name: str, user):
    """
    Generate files  .aln (multiple alignment) and  .dnd (guide tree).
    Call functions : get_names_aln_dnd () ,  get_title_file () and get_name_title_file()

    Arguments:

      file_name {str} -- path file

      user {user} -- user

    Returns:

      [tuple] -- Returns a boolean if there has been an error and returns
      the name of the file that was created
    """

    result = False
    clustalw_exe = r"./clustalw"
    clustalw_cline = ClustalwCommandline(clustalw_exe, infile=file_name)
    assert os.path.isfile(clustalw_exe), "Clustal W executable missing"
    stdout, stderr = clustalw_cline()
    thistuple = clustalw_cline()
    tuples0 = str(thistuple[0])
    tuple_names = get_names_aln_dnd(tuples0)

    Url.objects.create(title=get_title_file(
        tuple_names[0]), types='Guide Tree', user=user, gb=tuple_names[0], sequences=None)

    Url.objects.create(title=get_title_file(
        tuple_names[1]), types='Alignments', user=user, gb=tuple_names[1], sequences=None)

    name_title = get_name_title_file(get_title_file(tuple_names[0]))
    if stderr == "":
        result = True
    return (result, name_title)

# === Get_names_aln_dnd ===
def get_names_aln_dnd(cadena: str):
    """
    Give absolute paths of the files aln and dnd.

    Arguments:

      cadena {str} -- paths of the files

    Returns:

      [tuple] -- names of the files
    """

    listresult = []
    name1_search_tuple = re.search(
        r'file\s+created:\s+(\[.+\.dnd\])', cadena).group(1)
    listresult.append(name1_search_tuple)
    name2_search_tuple = re.search(
        r'CLUSTAL-Alignment\s+file\s+created\s+(\[.+\.aln\])', cadena).group(1)
    listresult.append(name2_search_tuple)

    list_names_split = [n.split('[') for n in listresult]
    l_name1 = list_names_split[0][1].split(']')[0]
    l_name2 = list_names_split[1][1].split(']')[0]
    return (l_name1, l_name2)

# === Get_title_file ===
def get_title_file(path: str):
    """
    Give the names of the files aln and dnd.

    Arguments:

      path {str} -- path of file

    Returns:

      [str] -- name of the file
    """

    name = ""
    pattern_name = re.compile(r'media\/.+\/(.+)')
    prog_name = re.compile(pattern_name)
    result_name = prog_name.match(path)
    name = result_name.group(1)
    return name

# === Get_name_title_file ===
def get_name_title_file(path: str):
    """
    Gives the name of a file, without the extension .dnd

    Arguments:

      path {str} -- name file with extension

    Returns:

      [str] --  file name without extension
    """

    name = ""
    pattern_name = re.compile(r'(.+)\.dnd')
    prog_name = re.compile(pattern_name)
    result_name = prog_name.match(path)
    name = result_name.group(1)
    return name

# === Random_string ===
def random_string(string_length=8):
    """
    Generate random names unique

    Keyword Arguments:

      string_length {int} -- name (default: {8})

    Returns:

      [str] -- random name
    """

    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(string_length))


# === Write_file_sequences ===
def write_file_sequences(sequences: str, user):
    """
    Create a file when the user enters sequences.
    Call functions :  random_string ()

    Arguments:

      sequences {str} -- sequences

      user {[type]} -- user

    Returns:

      [str] -- new path file
    """

    os.makedirs('media/user_'+str(user.id), exist_ok=True)
    file_suffix = random_string(15)
    name_file = 'seq_' + str(user.id) + file_suffix + '.fasta'
    path_file = 'media/user_' + str(user.id) + '/' + name_file

    with open(path_file, 'w') as fil:
        fil.write(sequences)

    Url.objects.create(title=name_file, types='MultiFasta',
                       user=user, gb=path_file, sequences=sequences)

    return path_file

# === Delete_dir ===
def delete_dir(path: str):
    """
    Delete all files of directory

    Arguments:

      path {str} -- path file

    Returns:

      [] -- delete file
    """

    dir_path = path

    try:
        shutil.rmtree(dir_path)
        return ""
    except OSError as error:
        return "Error: %s : %s" % (dir_path, error.strerror)

# === Validate_file ===
def validate_file(filename: str):
    """
    Validate if file have 2 sequences minim (>) and are unique names and is format .fasta

    Call functions : is_fasta(), num_seq ()

    Arguments:

      filename {str} -- path file

    Returns:

      [boolean] -- validation
    """

    if is_fasta(filename) == True and num_seq(filename) == True:
        return True
    return False

# === Is_fasta ===
def is_fasta(filename: str):
    """
    Format .fasta

    Arguments:

      filename {str} -- path file

    Returns:

      [boolean] -- fasta
    """

    with open(filename, "r") as handle:
        fasta = SeqIO.parse(handle, "fasta")
        return any(fasta)

# === Is_fasta_extension ===
def is_fasta_extension(name_file: str):
    """
    Check if the file extension is .fasta, .fa, .fna, .ffn, .faa, .frn

    Arguments:

      name_file {str} -- name of file

    Returns:

      [boolean] -- valid
    """

    extension = ['fasta', 'fa', 'fna', 'ffn', 'faa', 'frn']
    valid = False

    for ext in extension:
        pattern_name = re.compile(r'.+(\.'+ext+')$')
        prog_name = re.compile(pattern_name)
        result_name = prog_name.match(name_file)

        if result_name != None:
            valid = True
            return valid
    return valid

# === Empty_sequences ===
def empty_sequences(filename: str):
    """
    Check if sequences are empty

    Call functions : len_seq_ids()

    Arguments:

      filename {str} -- path file

    Returns:

      [boolean] -- check sequences
    """

    fil = open(filename, "r")
    txt = fil.read()

    result = len_seq_ids(txt)

    fil.close()

    return result

# === Len_seq_ids ===
def len_seq_ids(txt: str):
    """
    Check if the number of times the character > is equal to the number of
    sequences (the text between two characters >)

    Call functions: names_empty()

    Arguments:

      txt {str} -- text

    Returns:

      [boolean] -- check
    """

    list_seq2 = []
    list_seq3 = []
    list_ids = []
    list_ids = re.findall('>(.*)\n', txt)

    regex = r'^>(.*)[^>]'
    pat = re.compile(regex, re.MULTILINE)
    txt_sub = pat.sub("*", txt)
    list_txt_split = txt_sub.split("*")

    lenght = len(list_txt_split)
    for i in range(lenght):
        if list_txt_split[i] != '':
            list_seq2.append(list_txt_split[i].replace('\n', ''))

    for i in range(len(list_seq2)):
        if list_seq2[i] != '':
            list_seq3.append(list_seq2[i])

    check_names = names_empty(list_ids)

    check_list = "".join(list_seq3)
    check_s2 = check_list.isalpha()

    if len(list_ids) == len(list_seq3) and check_s2 == True and check_names == False:
        return True
    return False

# === Names_empty ===
def names_empty(list_ids: list):
    """
    Value empty in a list

    Call functions: is_blanck()

    Arguments:

      list_ids {list} -- id list

    Returns:

      [boolean] -- flat
    """

    flat = False
    for i in range(len(list_ids)):
        if is_blanck(list_ids[i]) == True:
            flat = True
    return flat

# === Is_blanck ===
def is_blanck(my_str):
    """
    String empty

    Arguments:

      my_str {str} -- string

    Returns:

      [boolean] -- if is blank
    """
    if my_str and my_str.strip():
        return False
    return True

# === Num_seq ===
def num_seq(filename: str):
    """
    The number of sequences in the file is given by the number
    of times the character appears >

    Call functions : unique_values ()

    Arguments:

      filename {str} -- path file

    Returns:

      [boolean] -- valid
    """

    valid = False
    numseq = 0
    list_names_seq = []
    for line in open(filename):
        line = line.rstrip()
        if re.match("^>", line):
            numseq += 1
            list_names_seq.append(line)

    if unique_values(list_names_seq) == True and numseq >= 2:
        valid = True

    return valid

# === Unique_values ===
def unique_values(listg):
    """
    The content after the character  > must be unique

    Arguments:

      listg {list} -- list of sequence names

    Returns:

      [boolean] -- check
    """

    seti = set()
    for value in listg:
        if value in seti:
            return False
        seti.add(value)
    return True
