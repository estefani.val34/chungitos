#!/usr/bin/env python
"""
Program which recreates the Needleman Wunsch alignment.
Currently we support the following 3 functions:

1. **Get_matrix** - Create the matrix of sequences

2. **Get_max_score** - Return max score
3. **Get_alignment** - Return alignment

"""
import numpy as np


# === get_matrix ===
def get_matrix(secuencia1: str, secuencia2: str):
    """
    Create the matrix of sequence 1 and  of sequence 2

    Arguments:

      secuencia2 {str} -- sequence one

      secuencia1 {str} -- sequence two

    Returns:

      [np.ndarray] -- matrix of alignment
    """

    filas = len(secuencia2)
    columnas = len(secuencia1)
    matrix = np.zeros([filas+1, columnas+1], int)
    penalty = -1
    for i in range(filas+1):
        matrix[i, 0] = penalty*i
    for j in range(columnas+1):
        matrix[0, j] = penalty*j
    for j in range(columnas):
        for i in range(filas):
            letra2 = secuencia2[i]
            letra1 = secuencia1[j]
            if letra2 == letra1:
                concurrence = 1
            else:
                concurrence = -1
            fat = max(matrix[i, j]+concurrence,
                      matrix[i+1, j]+penalty, matrix[i, j+1]+penalty)
            matrix[i+1, j+1] = fat
    return matrix

# === get_max_score ===


def get_max_score(matrix: np.ndarray):
    """
    Return max score

    Arguments:

      matrix {np.ndarray} -- matrix of alignment

    Returns:
      [int] -- max score
    """

    score = matrix[matrix.shape[0]-1, matrix.shape[1]-1]
    return score

# === get_alignment ===


def get_alignment(matrix: np.ndarray, secuencia1: str, secuencia2: str):
    """
    Return alignment

    Arguments:

      matrix {np.ndarray} -- matrix of alignment

      secuencia1 {str} -- sequence one

      secuencia2 {str} -- sequence two

    Returns:
      [list] -- alignment
    """

    result = []
    i = len(secuencia2)
    j = len(secuencia1)
    string1 = ""
    string1_list = []
    string2 = ""
    string2_list = []
    string3 = ""
    string3_list = []

    penalty = -1
    while(i > 0 or j > 0):
        if secuencia2[i-1] == secuencia1[j-1]:
            concurrence = 1
        else:
            concurrence = -1
        if i > 0 and j > 0 and matrix[i, j] == matrix[i-1, j-1]+concurrence:
            string1 = string1+secuencia1[j-1]
            string1_list.append((string1+secuencia1[j-1]).upper())
            string2 = string2+secuencia2[i-1]
            string2_list.append((string2+secuencia2[i-1]).upper())
            string3 = string3+"|"
            string3_list.append((string3+"|").upper())
            i = i-1
            j = j-1
        elif i > 0 and matrix[i, j] == matrix[i-1, j]+penalty:
            string1 = string1+"-"
            string1_list.append((string1+"-").upper())
            string2 = string2+secuencia2[i-1]
            string2_list.append((string2+secuencia2[i-1]).upper())
            string3 = string3+" "
            string3_list.append((string3+" ").upper())
            i = i-1
        else:
            string1 = string1+secuencia1[j-1]
            string1_list.append((string1+secuencia1[j-1]).upper())
            string2 = string2+"-"
            string2_list.append((string2+"-").upper())
            string3 = string3+" "
            string3_list.append((string3+" ").upper())
            j = j-1

    result = [string1[::-1], string3[::-1], string2[::-1]]
    result = [x.upper() for x in result]
    return result
