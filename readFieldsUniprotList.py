"""
Fill the database tables with Uniprot values 

References:

https://www.uniprot.org/uniprot
"""

import django
import os.path
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mysiteproyect.settings")
django.setup()
import re
from requests import get
from proteins.models import Protein, PBD


list_files = ['P31946', 'P62258', 'Q04917', 'P61981', 'P31947',
              'P27348', 'P63104', 'Q4AC99', 'Q15172', 'Q14738', 'Q16537']

pat = './infiles'


def download(url, file_name):
    with open(os.path.join(pat, file_name), "wb") as file:
        response = get(url)
        file.write(response.content)


"""
read file txt
"""
for infile in list_files:
    file_name = infile+'.txt'
    url = 'https://www.uniprot.org/uniprot/'+file_name
    download(url, file_name)

    with open(os.path.join(pat, file_name), "r") as txt_file:
        f1 = txt_file.readlines()

    p = Protein()

    """
    entry
    """
    entry = ""
    pattern_entry = re.compile(r'AC\s+(\w+)')
    prog_entry = re.compile(pattern_entry)
    result_entry = prog_entry.match(f1[1])
    entry = result_entry.group(1)

    p.entry = entry
    """
    entry_name
    """
    entry_name = ""
    pattern = re.compile(r'ID (.+) Reviewed.+')
    prog = re.compile(pattern)
    result = prog.match(f1[0])

    entry_name = result.group(1).strip()

    p.entry_name = entry_name

    """
    protein_names
    """
    protein_names_list = []
    protein_names = ""

    pattern_protein_names = re.compile(r'DE\s+.+Full=(.+);')

    for i in range(0, len(f1)):
        for m in re.finditer(pattern_protein_names, f1[i]):
            if m.group(1) not in protein_names_list:
                protein_names_list.append(m.group(1))

    protein_names = ". ".join(protein_names_list)
    p.protein_names = protein_names

    """
     cross_reference_GeneID
    """
    cross_reference_geneID_list = []
    cross_reference_geneID = ""

    pattern_geneid = re.compile('DR\s+GeneID;\s(\w+);')

    for i in range(0, len(f1)):
        for m in re.finditer(pattern_geneid, f1[i]):
            if m.group(1) not in cross_reference_geneID_list:
                cross_reference_geneID_list.append(m.group(1))

    cross_reference_geneID = " ".join(cross_reference_geneID_list)
    p.cross_reference_GeneID = cross_reference_geneID

    """
    gene_names
    """
    gen_name_list = []
    gen_name = ""

    pattern_gen_name = re.compile('GN\s+Name=(.+);')

    for i in range(0, len(f1)):
        for m in re.finditer(pattern_gen_name, f1[i]):
            if m.group(1) not in gen_name_list:
                gen_name_list.append(m.group(1))

    gen_name = "".join(gen_name_list)

    p.gene_names = gen_name

    """
    organism
    """

    organism_list = []
    organism = ""

    pattern_organism = re.compile('OS\s+(.+).')

    for i in range(0, len(f1)):
        for m in re.finditer(pattern_organism, f1[i]):
            if m.group(1) not in organism_list:
                organism_list.append(m.group(1))

    organism = "".join(organism_list)
    p.organism = organism

    """
    taxonomy_lineage
    """

    taxo_list = []

    pattern_taxo = re.compile('OC\s+(.+)')

    for i in range(0, len(f1)):
        for m in re.finditer(pattern_taxo, f1[i]):
            if m.group(1) not in taxo_list:
                taxo_list.append(m.group(1))

    taxo1 = "".join(taxo_list)
    taxo2 = taxo1.split(";")
    taxo3 = " ->".join(taxo2)

    p.taxonomy_lineage = taxo3

    """
     sequence
    """
    seq_list = []
    pattern_seq = re.compile('SQ\s+SEQUENCE.+;')
    index_inicio = 0

    for i in range(0, len(f1)):
        for m in re.finditer(pattern_seq, f1[i]):
            index_inicio = i

    seq_list = f1[index_inicio+1:len(f1)-1]

    seq_list_split_join = []
    for l in seq_list:
        seq_list_split_join.append("".join(l.split()))

    seq = ""
    seq = "".join(seq_list_split_join)

    p.sequence = seq

    """
    length
    """
    length = ""
    pattern_length = re.compile(r'ID\s.+ (\d+)\s+AA')
    prog_length = re.compile(pattern_length)
    result_length = prog_length.match(f1[0])
    length = result_length.group(1)

    p.length = length

    """
    Cross_reference_PDB
    """
    pattern_function_inicio = re.compile(r'CC\s+-!-\s(FUNCTION).+')
    index_inicio_function = 0

    for i in range(0, len(f1)):
        for m in re.finditer(pattern_function_inicio, f1[i]):
            index_inicio_function = i

    function_list = f1[index_inicio_function+1:]
    f_idx = [i for i, item in enumerate(
        function_list) if re.search('CC\s+-!-\s+.+:', item)]

    f_idx_DR = [i for i, item in enumerate(
        function_list) if re.search(r'DR\s+PDB\s*', item)]
    f_DR = [i for i, item in enumerate(function_list) if re.search(
        r'DR\s+PDB\s*;\s*[0-9A-Z]+\s*;\s*.*', item)]
    cross_reference_PDB = ""

    if len(f_DR) != 0:

        if f_idx_DR.index(f_DR[-1]) < len(f_idx_DR)-1:
            index_fin_pdb = f_idx_DR[f_idx_DR.index(f_DR[-1])+1]
            act_list_join = " ".join(function_list[f_DR[0]:index_fin_pdb])
        else:
            act_list_join = " ".join(function_list[f_DR[0]])

        rep1 = re.sub('DR\s+', ' ', act_list_join)
        rep2 = re.sub('\n', ' ', rep1)
        rep3 = rep2.strip()
        cross_reference_PDB = rep3
        p.cross_reference_PDB = cross_reference_PDB

    else:

        p.cross_reference_PDB = cross_reference_PDB

    """
    save changes in the protein
    """
    p.save()

    """
    fill model PBD
    """

    lcross = cross_reference_PDB.split('PDB;')
    en = entry

    for pbd in lcross:
        if (pbd != ""):
            id_pbd = pbd.split(";")[0].strip()
            desc = ";".join(pbd.split(";")[1:]).strip()
            PBD.objects.create(id_pbd=id_pbd, entry=en,
                               protein=p, description=desc)

