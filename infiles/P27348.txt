ID   1433T_HUMAN             Reviewed;         245 AA.
AC   P27348; D6W4Z5; Q567U5; Q5TZU8; Q9UP48;
DT   01-AUG-1992, integrated into UniProtKB/Swiss-Prot.
DT   01-AUG-1992, sequence version 1.
DT   22-APR-2020, entry version 217.
DE   RecName: Full=14-3-3 protein theta;
DE   AltName: Full=14-3-3 protein T-cell;
DE   AltName: Full=14-3-3 protein tau;
DE   AltName: Full=Protein HS1;
GN   Name=YWHAQ;
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi; Mammalia;
OC   Eutheria; Euarchontoglires; Primates; Haplorrhini; Catarrhini; Hominidae;
OC   Homo.
OX   NCBI_TaxID=9606;
RN   [1]
RP   NUCLEOTIDE SEQUENCE [MRNA].
RC   TISSUE=T-cell;
RX   PubMed=2015305; DOI=10.1016/0167-4781(91)90136-a;
RA   Nielsen P.J.;
RT   "Primary structure of a human protein kinase regulator protein.";
RL   Biochim. Biophys. Acta 1088:425-428(1991).
RN   [2]
RP   NUCLEOTIDE SEQUENCE [MRNA].
RC   TISSUE=Keratinocyte;
RX   PubMed=8515476; DOI=10.1006/jmbi.1993.1346;
RA   Leffers H., Madsen P., Rasmussen H.H., Honore B., Andersen A.H., Walbum E.,
RA   Vandekerckhove J., Celis J.E.;
RT   "Molecular cloning and expression of the transformation sensitive
RT   epithelial marker stratifin. A member of a protein family that has been
RT   involved in the protein kinase C signalling pathway.";
RL   J. Mol. Biol. 231:982-998(1993).
RN   [3]
RP   NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA].
RA   Kalnine N., Chen X., Rolfs A., Halleck A., Hines L., Eisenstein S.,
RA   Koundinya M., Raphael J., Moreira D., Kelley T., LaBaer J., Lin Y.,
RA   Phelan M., Farmer A.;
RT   "Cloning of human full-length CDSs in BD Creator(TM) system donor vector.";
RL   Submitted (OCT-2004) to the EMBL/GenBank/DDBJ databases.
RN   [4]
RP   NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA].
RA   Mural R.J., Istrail S., Sutton G.G., Florea L., Halpern A.L., Mobarry C.M.,
RA   Lippert R., Walenz B., Shatkay H., Dew I., Miller J.R., Flanigan M.J.,
RA   Edwards N.J., Bolanos R., Fasulo D., Halldorsson B.V., Hannenhalli S.,
RA   Turner R., Yooseph S., Lu F., Nusskern D.R., Shue B.C., Zheng X.H.,
RA   Zhong F., Delcher A.L., Huson D.H., Kravitz S.A., Mouchard L., Reinert K.,
RA   Remington K.A., Clark A.G., Waterman M.S., Eichler E.E., Adams M.D.,
RA   Hunkapiller M.W., Myers E.W., Venter J.C.;
RL   Submitted (SEP-2005) to the EMBL/GenBank/DDBJ databases.
RN   [5]
RP   NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA].
RC   TISSUE=Placenta, Skin, and Uterus;
RX   PubMed=15489334; DOI=10.1101/gr.2596504;
RG   The MGC Project Team;
RT   "The status, quality, and expansion of the NIH full-length cDNA project:
RT   the Mammalian Gene Collection (MGC).";
RL   Genome Res. 14:2121-2127(2004).
RN   [6]
RP   PROTEIN SEQUENCE OF 1-18.
RC   TISSUE=Platelet;
RX   PubMed=12665801; DOI=10.1038/nbt810;
RA   Gevaert K., Goethals M., Martens L., Van Damme J., Staes A., Thomas G.R.,
RA   Vandekerckhove J.;
RT   "Exploring proteomes and analyzing protein processing by mass spectrometric
RT   identification of sorted N-terminal peptides.";
RL   Nat. Biotechnol. 21:566-569(2003).
RN   [7]
RP   PROTEIN SEQUENCE OF 1-9; 12-55; 61-74; 128-157; 168-193 AND 213-222,
RP   INTERACTION WITH PI4KB; TBC1D22A AND TBC1D22B, AND IDENTIFICATION BY MASS
RP   SPECTROMETRY.
RX   PubMed=23572552; DOI=10.1128/mbio.00098-13;
RA   Greninger A.L., Knudsen G.M., Betegon M., Burlingame A.L., DeRisi J.L.;
RT   "ACBD3 interaction with TBC1 domain 22 protein is differentially affected
RT   by enteroviral and kobuviral 3A protein binding.";
RL   MBio 4:E00098-E00098(2013).
RN   [8]
RP   PROTEIN SEQUENCE OF 1-9; 28-49; 61-68; 104-115; 139-167 AND 213-222,
RP   ACETYLATION AT MET-1, AND IDENTIFICATION BY MASS SPECTROMETRY.
RC   TISSUE=B-cell lymphoma, and Hepatoma;
RA   Bienvenut W.V., Dhillon A.S., Kolch W.;
RL   Submitted (FEB-2008) to UniProtKB.
RN   [9]
RP   NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA] OF 73-245.
RC   TISSUE=Brain;
RA   Yu W., Gibbs R.A.;
RL   Submitted (JUN-1998) to the EMBL/GenBank/DDBJ databases.
RN   [10]
RP   PHOSPHORYLATION AT SER-232, AND IDENTIFICATION BY MASS SPECTROMETRY.
RX   PubMed=9360956; DOI=10.1074/jbc.272.46.28882;
RA   Dubois T., Rommel C., Howell S., Steinhussen U., Soneji Y., Morrice N.,
RA   Moelling K., Aitken A.;
RT   "14-3-3 is phosphorylated by casein kinase I on residue 233.
RT   Phosphorylation at this site in vivo regulates Raf/14-3-3 interaction.";
RL   J. Biol. Chem. 272:28882-28888(1997).
RN   [11]
RP   INTERACTION WITH RGS7.
RX   PubMed=10862767; DOI=10.1074/jbc.m002905200;
RA   Benzing T., Yaffe M.B., Arnould T., Sellin L., Schermer B., Schilling B.,
RA   Schreiber R., Kunzelmann K., Leparc G.G., Kim E., Walz G.;
RT   "14-3-3 interacts with regulator of G protein signaling proteins and
RT   modulates their activity.";
RL   J. Biol. Chem. 275:28167-28172(2000).
RN   [12]
RP   TISSUE SPECIFICITY.
RX   PubMed=11080204; DOI=10.1046/j.1471-4159.2000.0752511.x;
RA   Malaspina A., Kaushik N., de Belleroche J.;
RT   "A 14-3-3 mRNA is up-regulated in amyotrophic lateral sclerosis spinal
RT   cord.";
RL   J. Neurochem. 75:2511-2520(2000).
RN   [13]
RP   INTERACTION WITH CDKN1B.
RX   PubMed=12042314; DOI=10.1074/jbc.m203668200;
RA   Fujita N., Sato S., Katayama K., Tsuruo T.;
RT   "Akt-dependent phosphorylation of p27Kip1 promotes binding to 14-3-3 and
RT   cytoplasmic localization.";
RL   J. Biol. Chem. 277:28706-28713(2002).
RN   [14]
RP   FUNCTION, AND INTERACTION WITH PDPK1.
RX   PubMed=12177059; DOI=10.1074/jbc.m205141200;
RA   Sato S., Fujita N., Tsuruo T.;
RT   "Regulation of kinase activity of 3-phosphoinositide-dependent protein
RT   kinase-1 by binding to 14-3-3.";
RL   J. Biol. Chem. 277:39360-39367(2002).
RN   [15]
RP   IDENTIFICATION BY MASS SPECTROMETRY.
RC   TISSUE=Lymphoblast;
RX   PubMed=14654843; DOI=10.1038/nature02166;
RA   Andersen J.S., Wilkinson C.J., Mayor T., Mortensen P., Nigg E.A., Mann M.;
RT   "Proteomic characterization of the human centrosome by protein correlation
RT   profiling.";
RL   Nature 426:570-574(2003).
RN   [16]
RP   INTERACTION WITH SSH1.
RX   PubMed=15159416; DOI=10.1083/jcb.200401136;
RA   Nagata-Ohashi K., Ohta Y., Goto K., Chiba S., Mori R., Nishita M.,
RA   Ohashi K., Kousaka K., Iwamatsu A., Niwa R., Uemura T., Mizuno K.;
RT   "A pathway of neuregulin-induced activation of cofilin-phosphatase
RT   Slingshot and cofilin in lamellipodia.";
RL   J. Cell Biol. 165:465-471(2004).
RN   [17]
RP   IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS].
RC   TISSUE=Cervix carcinoma;
RX   PubMed=17081983; DOI=10.1016/j.cell.2006.09.026;
RA   Olsen J.V., Blagoev B., Gnad F., Macek B., Kumar C., Mortensen P., Mann M.;
RT   "Global, in vivo, and site-specific phosphorylation dynamics in signaling
RT   networks.";
RL   Cell 127:635-648(2006).
RN   [18]
RP   INTERACTION WITH MARK2; MARK3 AND MARK4.
RX   PubMed=16959763; DOI=10.1074/mcp.m600147-mcp200;
RA   Angrand P.O., Segura I., Voelkel P., Ghidelli S., Terry R., Brajenovic M.,
RA   Vintersten K., Klein R., Superti-Furga G., Drewes G., Kuster B.,
RA   Bouwmeester T., Acker-Palmer A.;
RT   "Transgenic mouse proteomics identifies new 14-3-3-associated proteins
RT   involved in cytoskeletal rearrangements and cell signaling.";
RL   Mol. Cell. Proteomics 5:2211-2227(2006).
RN   [19]
RP   INTERACTION WITH GAB2.
RX   PubMed=19172738; DOI=10.1038/emboj.2008.159;
RA   Brummer T., Larance M., Herrera Abreu M.T., Lyons R.J., Timpson P.,
RA   Emmerich C.H., Fleuren E.D.G., Lehrbach G.M., Schramek D., Guilhaus M.,
RA   James D.E., Daly R.J.;
RT   "Phosphorylation-dependent binding of 14-3-3 terminates signalling by the
RT   Gab2 docking protein.";
RL   EMBO J. 27:2305-2316(2008).
RN   [20]
RP   IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS].
RC   TISSUE=Cervix carcinoma;
RX   PubMed=18691976; DOI=10.1016/j.molcel.2008.07.007;
RA   Daub H., Olsen J.V., Bairlein M., Gnad F., Oppermann F.S., Korner R.,
RA   Greff Z., Keri G., Stemmann O., Mann M.;
RT   "Kinase-selective enrichment enables quantitative phosphoproteomics of the
RT   kinome across the cell cycle.";
RL   Mol. Cell 31:438-448(2008).
RN   [21]
RP   PHOSPHORYLATION [LARGE SCALE ANALYSIS] AT SER-232, AND IDENTIFICATION BY
RP   MASS SPECTROMETRY [LARGE SCALE ANALYSIS].
RC   TISSUE=Cervix carcinoma;
RX   PubMed=18669648; DOI=10.1073/pnas.0805139105;
RA   Dephoure N., Zhou C., Villen J., Beausoleil S.A., Bakalarski C.E.,
RA   Elledge S.J., Gygi S.P.;
RT   "A quantitative atlas of mitotic phosphorylation.";
RL   Proc. Natl. Acad. Sci. U.S.A. 105:10762-10767(2008).
RN   [22]
RP   INTERACTION WITH SLITRK1.
RX   PubMed=19640509; DOI=10.1016/j.biopsych.2009.05.033;
RA   Kajiwara Y., Buxbaum J.D., Grice D.E.;
RT   "SLITRK1 binds 14-3-3 and regulates neurite outgrowth in a phosphorylation-
RT   dependent manner.";
RL   Biol. Psychiatry 66:918-925(2009).
RN   [23]
RP   IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS].
RC   TISSUE=Leukemic T-cell;
RX   PubMed=19690332; DOI=10.1126/scisignal.2000007;
RA   Mayya V., Lundgren D.H., Hwang S.-I., Rezaul K., Wu L., Eng J.K.,
RA   Rodionov V., Han D.K.;
RT   "Quantitative phosphoproteomic analysis of T cell receptor signaling
RT   reveals system-wide modulation of protein-protein interactions.";
RL   Sci. Signal. 2:RA46-RA46(2009).
RN   [24]
RP   ACETYLATION [LARGE SCALE ANALYSIS] AT LYS-3; LYS-49; LYS-68 AND LYS-115,
RP   AND IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS].
RX   PubMed=19608861; DOI=10.1126/science.1175371;
RA   Choudhary C., Kumar C., Gnad F., Nielsen M.L., Rehman M., Walther T.C.,
RA   Olsen J.V., Mann M.;
RT   "Lysine acetylation targets protein complexes and co-regulates major
RT   cellular functions.";
RL   Science 325:834-840(2009).
RN   [25]
RP   IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS].
RC   TISSUE=Cervix carcinoma;
RX   PubMed=20068231; DOI=10.1126/scisignal.2000475;
RA   Olsen J.V., Vermeulen M., Santamaria A., Kumar C., Miller M.L.,
RA   Jensen L.J., Gnad F., Cox J., Jensen T.S., Nigg E.A., Brunak S., Mann M.;
RT   "Quantitative phosphoproteomics reveals widespread full phosphorylation
RT   site occupancy during mitosis.";
RL   Sci. Signal. 3:RA3-RA3(2010).
RN   [26]
RP   IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS].
RX   PubMed=21269460; DOI=10.1186/1752-0509-5-17;
RA   Burkard T.R., Planyavsky M., Kaupe I., Breitwieser F.P., Buerckstuemmer T.,
RA   Bennett K.L., Superti-Furga G., Colinge J.;
RT   "Initial characterization of the human central proteome.";
RL   BMC Syst. Biol. 5:17-17(2011).
RN   [27]
RP   ACETYLATION [LARGE SCALE ANALYSIS] AT MET-1, AND IDENTIFICATION BY MASS
RP   SPECTROMETRY [LARGE SCALE ANALYSIS].
RX   PubMed=22814378; DOI=10.1073/pnas.1210303109;
RA   Van Damme P., Lasa M., Polevoda B., Gazquez C., Elosegui-Artola A.,
RA   Kim D.S., De Juan-Pardo E., Demeyer K., Hole K., Larrea E., Timmerman E.,
RA   Prieto J., Arnesen T., Sherman F., Gevaert K., Aldabe R.;
RT   "N-terminal acetylome analyses and functional insights of the N-terminal
RT   acetyltransferase NatB.";
RL   Proc. Natl. Acad. Sci. U.S.A. 109:12449-12454(2012).
RN   [28]
RP   IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS].
RC   TISSUE=Liver;
RX   PubMed=24275569; DOI=10.1016/j.jprot.2013.11.014;
RA   Bian Y., Song C., Cheng K., Dong M., Wang F., Huang J., Sun D., Wang L.,
RA   Ye M., Zou H.;
RT   "An enzyme assisted RP-RPLC approach for in-depth analysis of human liver
RT   phosphoproteome.";
RL   J. Proteomics 96:253-262(2014).
RN   [29]
RP   INTERACTION WITH DAPK2.
RX   PubMed=26047703; DOI=10.1016/j.bbrc.2015.05.105;
RA   Yuasa K., Ota R., Matsuda S., Isshiki K., Inoue M., Tsuji A.;
RT   "Suppression of death-associated protein kinase 2 by interaction with 14-3-
RT   3 proteins.";
RL   Biochem. Biophys. Res. Commun. 464:70-75(2015).
RN   [30]
RP   INTERACTION WITH RIPOR2.
RX   PubMed=25588844; DOI=10.1242/jcs.161497;
RA   Gao K., Tang W., Li Y., Zhang P., Wang D., Yu L., Wang C., Wu D.;
RT   "Front-signal-dependent accumulation of the RHOA inhibitor FAM65B at
RT   leading edges polarizes neutrophils.";
RL   J. Cell Sci. 128:992-1000(2015).
RN   [31]
RP   ACETYLATION [LARGE SCALE ANALYSIS] AT MET-1, AND IDENTIFICATION BY MASS
RP   SPECTROMETRY [LARGE SCALE ANALYSIS].
RX   PubMed=25944712; DOI=10.1002/pmic.201400617;
RA   Vaca Jacome A.S., Rabilloud T., Schaeffer-Reiss C., Rompais M., Ayoub D.,
RA   Lane L., Bairoch A., Van Dorsselaer A., Carapito C.;
RT   "N-terminome analysis of the human mitochondrial proteome.";
RL   Proteomics 15:2519-2524(2015).
RN   [32]
RP   FUNCTION, AND INTERACTION WITH INAVA.
RX   PubMed=28436939; DOI=10.1172/jci86282;
RA   Yan J., Hedl M., Abraham C.;
RT   "An inflammatory bowel disease-risk variant in INAVA decreases pattern
RT   recognition receptor-induced outcomes.";
RL   J. Clin. Invest. 127:2192-2205(2017).
RN   [33]
RP   SUMOYLATION [LARGE SCALE ANALYSIS] AT LYS-49, AND IDENTIFICATION BY MASS
RP   SPECTROMETRY [LARGE SCALE ANALYSIS].
RX   PubMed=28112733; DOI=10.1038/nsmb.3366;
RA   Hendriks I.A., Lyon D., Young C., Jensen L.J., Vertegaal A.C.,
RA   Nielsen M.L.;
RT   "Site-specific mapping of the human SUMO proteome reveals co-modification
RT   with phosphorylation.";
RL   Nat. Struct. Mol. Biol. 24:325-336(2017).
RN   [34]
RP   X-RAY CRYSTALLOGRAPHY (2.6 ANGSTROMS).
RX   PubMed=7603573; DOI=10.1038/376188a0;
RA   Xiao B., Smerdon S.J., Jones D.H., Dodson G.G., Soneji Y., Aitken A.,
RA   Gamblin S.J.;
RT   "Structure of a 14-3-3 protein and implications for coordination of
RT   multiple signalling pathways.";
RL   Nature 376:188-191(1995).
RN   [35]
RP   X-RAY CRYSTALLOGRAPHY (2.8 ANGSTROMS) OF 1-234, IDENTIFICATION BY MASS
RP   SPECTROMETRY, INTERACTION WITH PHOSPHOSERINE MOTIFS, AND SUBUNIT.
RX   PubMed=17085597; DOI=10.1073/pnas.0605779103;
RA   Yang X., Lee W.H., Sobott F., Papagrigoriou E., Robinson C.V.,
RA   Grossmann J.G., Sundstroem M., Doyle D.A., Elkins J.M.;
RT   "Structural basis for protein-protein interactions in the 14-3-3 protein
RT   family.";
RL   Proc. Natl. Acad. Sci. U.S.A. 103:17237-17242(2006).
CC   -!- FUNCTION: Adapter protein implicated in the regulation of a large
CC       spectrum of both general and specialized signaling pathways. Binds to a
CC       large number of partners, usually by recognition of a phosphoserine or
CC       phosphothreonine motif. Binding generally results in the modulation of
CC       the activity of the binding partner. Negatively regulates the kinase
CC       activity of PDPK1. {ECO:0000269|PubMed:12177059}.
CC   -!- SUBUNIT: Homodimer. Interacts with CDK16 (By similarity). Interacts
CC       with RGS7 (phosphorylated form) (PubMed:10862767). Interacts with SSH1.
CC       Interacts with CDKN1B ('Thr-198' phosphorylated form); the interaction
CC       translocates CDKN1B to the cytoplasm. Interacts with GAB2. Interacts
CC       with the 'Ser-241' phosphorylated form of PDPK1. Interacts with the
CC       'Thr-369' phosphorylated form of DAPK2 (PubMed:26047703). Interacts
CC       with PI4KB, TBC1D22A and TBC1D22B (PubMed:23572552). Interacts with
CC       SLITRK1 (PubMed:19640509). Interacts with RIPOR2 isoform 2
CC       (PubMed:25588844). Interacts with INAVA; the interaction increases upon
CC       PRR (pattern recognition receptor) stimulation and is required for
CC       cellular signaling pathway activation and cytokine secretion
CC       (PubMed:28436939). Interacts with MARK2, MARK3 and MARK4
CC       (PubMed:16959763). {ECO:0000250|UniProtKB:P68254,
CC       ECO:0000269|PubMed:10862767, ECO:0000269|PubMed:12042314,
CC       ECO:0000269|PubMed:12177059, ECO:0000269|PubMed:15159416,
CC       ECO:0000269|PubMed:16959763, ECO:0000269|PubMed:17085597,
CC       ECO:0000269|PubMed:19172738, ECO:0000269|PubMed:19640509,
CC       ECO:0000269|PubMed:23572552, ECO:0000269|PubMed:25588844,
CC       ECO:0000269|PubMed:26047703, ECO:0000269|PubMed:28436939}.
CC   -!- INTERACTION:
CC       P27348; P27348; NbExp=2; IntAct=EBI-359854, EBI-359854;
CC       P27348; Q9P0K1-3: ADAM22; NbExp=2; IntAct=EBI-359854, EBI-1567267;
CC       P27348; P49407: ARRB1; NbExp=3; IntAct=EBI-359854, EBI-743313;
CC       P27348; P32121: ARRB2; NbExp=3; IntAct=EBI-359854, EBI-714559;
CC       P27348; Q92934: BAD; NbExp=5; IntAct=EBI-359854, EBI-700771;
CC       P27348; P22681: CBL; NbExp=6; IntAct=EBI-359854, EBI-518228;
CC       P27348; Q86Z20: CCDC125; NbExp=3; IntAct=EBI-359854, EBI-11977221;
CC       P27348; P30304: CDC25A; NbExp=3; IntAct=EBI-359854, EBI-747671;
CC       P27348; O94921: CDK14; NbExp=3; IntAct=EBI-359854, EBI-1043945;
CC       P27348; P67828: CSNK1A1; Xeno; NbExp=2; IntAct=EBI-359854, EBI-7540603;
CC       P27348; P00533: EGFR; NbExp=7; IntAct=EBI-359854, EBI-297353;
CC       P27348; P23945: FSHR; NbExp=4; IntAct=EBI-359854, EBI-848239;
CC       P27348; P56524: HDAC4; NbExp=3; IntAct=EBI-359854, EBI-308629;
CC       P27348; Q14678: KANK1; NbExp=2; IntAct=EBI-359854, EBI-2556221;
CC       P27348; Q14678-2: KANK1; NbExp=3; IntAct=EBI-359854, EBI-6173812;
CC       P27348; Q5S007: LRRK2; NbExp=10; IntAct=EBI-359854, EBI-5323863;
CC       P27348; Q99759: MAP3K3; NbExp=2; IntAct=EBI-359854, EBI-307281;
CC       P27348; P04049: RAF1; NbExp=6; IntAct=EBI-359854, EBI-365996;
CC       P27348; P61588: Rnd3; Xeno; NbExp=2; IntAct=EBI-359854, EBI-6930266;
CC       P27348; Q8WYL5: SSH1; NbExp=2; IntAct=EBI-359854, EBI-1222387;
CC       P27348; B7UM99: tir; Xeno; NbExp=6; IntAct=EBI-359854, EBI-2504426;
CC       P27348; Q8IWZ5: TRIM42; NbExp=4; IntAct=EBI-359854, EBI-5235829;
CC       P27348; P31946: YWHAB; NbExp=3; IntAct=EBI-359854, EBI-359815;
CC       P27348; P62258: YWHAE; NbExp=7; IntAct=EBI-359854, EBI-356498;
CC       P27348; P61981: YWHAG; NbExp=3; IntAct=EBI-359854, EBI-359832;
CC   -!- SUBCELLULAR LOCATION: Cytoplasm. Note=In neurons, axonally transported
CC       to the nerve terminals.
CC   -!- TISSUE SPECIFICITY: Abundantly expressed in brain, heart and pancreas,
CC       and at lower levels in kidney and placenta. Up-regulated in the lumbar
CC       spinal cord from patients with sporadic amyotrophic lateral sclerosis
CC       (ALS) compared with controls, with highest levels of expression in
CC       individuals with predominant lower motor neuron involvement.
CC       {ECO:0000269|PubMed:11080204}.
CC   -!- PTM: Ser-232 is probably phosphorylated by CK1.
CC       {ECO:0000269|PubMed:9360956}.
CC   -!- SIMILARITY: Belongs to the 14-3-3 family. {ECO:0000305}.
CC   ---------------------------------------------------------------------------
CC   Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms
CC   Distributed under the Creative Commons Attribution (CC BY 4.0) License
CC   ---------------------------------------------------------------------------
DR   EMBL; X56468; CAA39840.1; -; mRNA.
DR   EMBL; X57347; CAA40622.1; -; mRNA.
DR   EMBL; BT020014; AAV38817.1; -; mRNA.
DR   EMBL; CH471053; EAX00977.1; -; Genomic_DNA.
DR   EMBL; CH471053; EAX00979.1; -; Genomic_DNA.
DR   EMBL; BC050601; AAH50601.1; -; mRNA.
DR   EMBL; BC056867; AAH56867.1; -; mRNA.
DR   EMBL; BC093019; AAH93019.1; -; mRNA.
DR   EMBL; AF070556; AAC28640.1; -; mRNA.
DR   CCDS; CCDS1666.1; -.
DR   PIR; S15076; S15076.
DR   RefSeq; NP_006817.1; NM_006826.3.
DR   PDB; 2BTP; X-ray; 2.80 A; A/B=1-234.
DR   PDB; 5IQP; X-ray; 2.60 A; A/B=1-245.
DR   PDB; 6BCR; X-ray; 1.99 A; A/B/E/F=1-245.
DR   PDB; 6BD2; X-ray; 2.90 A; A/B=1-245.
DR   PDB; 6BQT; X-ray; 2.80 A; A/B/D/E/G/H/J/K=1-245.
DR   PDBsum; 2BTP; -.
DR   PDBsum; 5IQP; -.
DR   PDBsum; 6BCR; -.
DR   PDBsum; 6BD2; -.
DR   PDBsum; 6BQT; -.
DR   SMR; P27348; -.
DR   BioGrid; 116168; 523.
DR   CORUM; P27348; -.
DR   DIP; DIP-27584N; -.
DR   ELM; P27348; -.
DR   IntAct; P27348; 326.
DR   MINT; P27348; -.
DR   STRING; 9606.ENSP00000371267; -.
DR   BindingDB; P27348; -.
DR   ChEMBL; CHEMBL3710408; -.
DR   DrugBank; DB12695; Phenethyl Isothiocyanate.
DR   iPTMnet; P27348; -.
DR   MetOSite; P27348; -.
DR   PhosphoSitePlus; P27348; -.
DR   SwissPalm; P27348; -.
DR   BioMuta; YWHAQ; -.
DR   DMDM; 112690; -.
DR   OGP; P27348; -.
DR   REPRODUCTION-2DPAGE; IPI00018146; -.
DR   CPTAC; CPTAC-143; -.
DR   CPTAC; CPTAC-144; -.
DR   EPD; P27348; -.
DR   jPOST; P27348; -.
DR   MassIVE; P27348; -.
DR   MaxQB; P27348; -.
DR   PaxDb; P27348; -.
DR   PeptideAtlas; P27348; -.
DR   PRIDE; P27348; -.
DR   ProteomicsDB; 54380; -.
DR   TopDownProteomics; P27348; -.
DR   Antibodypedia; 1900; 548 antibodies.
DR   DNASU; 10971; -.
DR   Ensembl; ENST00000238081; ENSP00000238081; ENSG00000134308.
DR   Ensembl; ENST00000381844; ENSP00000371267; ENSG00000134308.
DR   GeneID; 10971; -.
DR   KEGG; hsa:10971; -.
DR   UCSC; uc002qzx.5; human.
DR   CTD; 10971; -.
DR   DisGeNET; 10971; -.
DR   GeneCards; YWHAQ; -.
DR   HGNC; HGNC:12854; YWHAQ.
DR   HPA; ENSG00000134308; Low tissue specificity.
DR   MIM; 609009; gene.
DR   neXtProt; NX_P27348; -.
DR   OpenTargets; ENSG00000134308; -.
DR   PharmGKB; PA37443; -.
DR   eggNOG; KOG0841; Eukaryota.
DR   eggNOG; COG5040; LUCA.
DR   GeneTree; ENSGT00970000193355; -.
DR   HOGENOM; CLU_058290_1_0_1; -.
DR   InParanoid; P27348; -.
DR   KO; K16197; -.
DR   OMA; RSAWRVM; -.
DR   OrthoDB; 1176818at2759; -.
DR   PhylomeDB; P27348; -.
DR   TreeFam; TF102002; -.
DR   Reactome; R-HSA-111447; Activation of BAD and translocation to mitochondria.
DR   Reactome; R-HSA-1445148; Translocation of SLC2A4 (GLUT4) to the plasma membrane.
DR   Reactome; R-HSA-5625740; RHO GTPases activate PKNs.
DR   Reactome; R-HSA-5628897; TP53 Regulates Metabolic Genes.
DR   Reactome; R-HSA-75035; Chk1/Chk2(Cds1) mediated inactivation of Cyclin B:Cdk1 complex.
DR   Reactome; R-HSA-9614399; Regulation of localization of FOXO transcription factors.
DR   SignaLink; P27348; -.
DR   SIGNOR; P27348; -.
DR   ChiTaRS; YWHAQ; human.
DR   EvolutionaryTrace; P27348; -.
DR   GeneWiki; YWHAQ; -.
DR   GenomeRNAi; 10971; -.
DR   Pharos; P27348; Tchem.
DR   PRO; PR:P27348; -.
DR   Proteomes; UP000005640; Chromosome 2.
DR   RNAct; P27348; protein.
DR   Bgee; ENSG00000134308; Expressed in substantia nigra and 244 other tissues.
DR   ExpressionAtlas; P27348; baseline and differential.
DR   Genevisible; P27348; HS.
DR   GO; GO:0005737; C:cytoplasm; IDA:BHF-UCL.
DR   GO; GO:0005829; C:cytosol; TAS:Reactome.
DR   GO; GO:0070062; C:extracellular exosome; HDA:UniProtKB.
DR   GO; GO:0005925; C:focal adhesion; HDA:UniProtKB.
DR   GO; GO:0016020; C:membrane; HDA:UniProtKB.
DR   GO; GO:0005739; C:mitochondrion; IEA:GOC.
DR   GO; GO:0032991; C:protein-containing complex; IDA:MGI.
DR   GO; GO:0045202; C:synapse; IEA:Ensembl.
DR   GO; GO:0071889; F:14-3-3 protein binding; IEA:Ensembl.
DR   GO; GO:0042802; F:identical protein binding; IPI:IntAct.
DR   GO; GO:0044325; F:ion channel binding; IEA:Ensembl.
DR   GO; GO:0008022; F:protein C-terminus binding; IEA:Ensembl.
DR   GO; GO:0019904; F:protein domain specific binding; IEA:Ensembl.
DR   GO; GO:0047485; F:protein N-terminus binding; IPI:UniProtKB.
DR   GO; GO:0061024; P:membrane organization; TAS:Reactome.
DR   GO; GO:0034766; P:negative regulation of ion transmembrane transport; IEA:Ensembl.
DR   GO; GO:0045892; P:negative regulation of transcription, DNA-templated; IDA:BHF-UCL.
DR   GO; GO:1900740; P:positive regulation of protein insertion into mitochondrial membrane involved in apoptotic signaling pathway; TAS:Reactome.
DR   GO; GO:0006605; P:protein targeting; IEA:Ensembl.
DR   GO; GO:0007264; P:small GTPase mediated signal transduction; IEA:Ensembl.
DR   GO; GO:0021762; P:substantia nigra development; HEP:UniProtKB.
DR   CDD; cd10023; 14-3-3_theta; 1.
DR   Gene3D; 1.20.190.20; -; 1.
DR   InterPro; IPR000308; 14-3-3.
DR   InterPro; IPR023409; 14-3-3_CS.
DR   InterPro; IPR036815; 14-3-3_dom_sf.
DR   InterPro; IPR023410; 14-3-3_domain.
DR   InterPro; IPR042584; 14-3-3_theta.
DR   PANTHER; PTHR18860; PTHR18860; 1.
DR   Pfam; PF00244; 14-3-3; 1.
DR   PIRSF; PIRSF000868; 14-3-3; 1.
DR   PRINTS; PR00305; 1433ZETA.
DR   SMART; SM00101; 14_3_3; 1.
DR   SUPFAM; SSF48445; SSF48445; 1.
DR   PROSITE; PS00796; 1433_1; 1.
DR   PROSITE; PS00797; 1433_2; 1.
PE   1: Evidence at protein level;
KW   3D-structure; Acetylation; Cytoplasm; Direct protein sequencing;
KW   Isopeptide bond; Nitration; Phosphoprotein; Reference proteome;
KW   Ubl conjugation.
FT   CHAIN           1..245
FT                   /note="14-3-3 protein theta"
FT                   /id="PRO_0000058636"
FT   SITE            56
FT                   /note="Interaction with phosphoserine on interacting
FT                   protein"
FT   SITE            127
FT                   /note="Interaction with phosphoserine on interacting
FT                   protein"
FT   MOD_RES         1
FT                   /note="N-acetylmethionine"
FT                   /evidence="ECO:0000244|PubMed:22814378,
FT                   ECO:0000244|PubMed:25944712, ECO:0000269|Ref.8"
FT   MOD_RES         3
FT                   /note="N6-acetyllysine"
FT                   /evidence="ECO:0000244|PubMed:19608861"
FT   MOD_RES         49
FT                   /note="N6-acetyllysine; alternate"
FT                   /evidence="ECO:0000244|PubMed:19608861"
FT   MOD_RES         68
FT                   /note="N6-acetyllysine"
FT                   /evidence="ECO:0000244|PubMed:19608861"
FT   MOD_RES         82
FT                   /note="Nitrated tyrosine"
FT                   /evidence="ECO:0000250|UniProtKB:Q9CQV8"
FT   MOD_RES         92
FT                   /note="Phosphoserine"
FT                   /evidence="ECO:0000250|UniProtKB:P68254"
FT   MOD_RES         104
FT                   /note="Nitrated tyrosine"
FT                   /evidence="ECO:0000250|UniProtKB:Q9CQV8"
FT   MOD_RES         115
FT                   /note="N6-acetyllysine"
FT                   /evidence="ECO:0000244|PubMed:19608861"
FT   MOD_RES         232
FT                   /note="Phosphoserine"
FT                   /evidence="ECO:0000244|PubMed:18669648,
FT                   ECO:0000269|PubMed:9360956"
FT   CROSSLNK        49
FT                   /note="Glycyl lysine isopeptide (Lys-Gly) (interchain with
FT                   G-Cter in SUMO2); alternate"
FT                   /evidence="ECO:0000244|PubMed:28112733"
FT   CONFLICT        136
FT                   /note="D -> N (in Ref. 3; AAV38817)"
FT                   /evidence="ECO:0000305"
FT   HELIX           3..16
FT                   /evidence="ECO:0000244|PDB:6BCR"
FT   HELIX           19..31
FT                   /evidence="ECO:0000244|PDB:6BCR"
FT   HELIX           38..68
FT                   /evidence="ECO:0000244|PDB:6BCR"
FT   HELIX           76..103
FT                   /evidence="ECO:0000244|PDB:6BCR"
FT   HELIX           105..108
FT                   /evidence="ECO:0000244|PDB:6BCR"
FT   HELIX           112..132
FT                   /evidence="ECO:0000244|PDB:6BCR"
FT   HELIX           136..159
FT                   /evidence="ECO:0000244|PDB:6BCR"
FT   HELIX           165..180
FT                   /evidence="ECO:0000244|PDB:6BCR"
FT   HELIX           185..200
FT                   /evidence="ECO:0000244|PDB:6BCR"
FT   HELIX           201..205
FT                   /evidence="ECO:0000244|PDB:6BCR"
FT   HELIX           208..228
FT                   /evidence="ECO:0000244|PDB:6BCR"
SQ   SEQUENCE   245 AA;  27764 MW;  175534325E9E37C4 CRC64;
     MEKTELIQKA KLAEQAERYD DMATCMKAVT EQGAELSNEE RNLLSVAYKN VVGGRRSAWR
     VISSIEQKTD TSDKKLQLIK DYREKVESEL RSICTTVLEL LDKYLIANAT NPESKVFYLK
     MKGDYFRYLA EVACGDDRKQ TIDNSQGAYQ EAFDISKKEM QPTHPIRLGL ALNFSVFYYE
     ILNNPELACT LAKTAFDEAI AELDTLNEDS YKDSTLIMQL LRDNLTLWTS DSAGEECDAA
     EGAEN
//
